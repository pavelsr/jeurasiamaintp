<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Add JavaScript Frameworks
//JHtml::_('bootstrap.framework');

// Add Stylesheets
//$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css');

// Load optional rtl Bootstrap css and Bootstrap bugfixes
//JHtmlBootstrap::loadCss($includeMaincss = false, $this->direction);
$id = JRequest::getVar('id', null);
$option = JRequest::getVar('option', null);
$pag = JRequest::getVar('pag', null);
$lang = JRequest::getVar('lang', null);
$task = JRequest::getVar('task', null);
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
<jdoc:include type="head" />
<?php
	//unset(	$this->_scripts[$this->baseurl.'/components/com_k2/js/k2.js?v2.6.9&amp;sitepath=/']);
 ?>
	<?php if ($pag == '1') { ?>
    <style type="text/css">
	#map_canvas {width:709px !important; height:340px !important;}
	</style>
	<?php } else if ($pag == '7') { ?>
    <style type="text/css">
	#map_canvas {width:380px !important; height:300px !important;}
	</style>
	<?php } ?>
</head>
	<?php if (($id == '9') and ($option == 'com_content')) { ?>
	<body style="margin:0; padding:0; overflow:hidden;">
	<?php if ($lang == 'en-GB') {
	$lang_z = 'en-US';
	} else if ($lang == 'fr-FR') {
	$lang_z = 'en-US';
	// pkl $lang_z = 'fr-FR';
	} else if ($lang == 'de-DE') {
	$lang_z = 'de-DE';
	} else {
	$lang_z = 'ru-RU';
	}
	?>


<link rel="stylesheet" type="text/css" href="templates/main/css/template.css">
	<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
	<?php
	 if ($lang == 'en-GB') {
	$query_dynasty = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=138";
	} else if ($lang == 'fr-FR') {
	$query_dynasty = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=236";
	} else if ($lang == 'de-DE') {
	$query_dynasty = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=335";
	 } else {
	$query_dynasty = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=51";
	 }
	$db =& JFactory::getDBO();
	$db->setQuery($query_dynasty);
	$res_dynasty = json_decode($db->loadResult(),true);

	 if ($lang == 'en-GB') {
	$query_privilegia = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=146";
	} else if ($lang == 'fr-FR') {
	$query_privilegia = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=244";
	} else if ($lang == 'de-DE') {
	$query_privilegia = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=333";
	 } else {
	$query_privilegia = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=59";
	 }
	$db =& JFactory::getDBO();
	$db->setQuery($query_privilegia);
	$res_privilegia = json_decode($db->loadResult(),true);

	 if ($lang == 'en-GB') {
	$query_amsterdam = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=155";
	} else if ($lang == 'fr-FR') {
	$query_amsterdam = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=253";
	} else if ($lang == 'de-DE') {
	$query_amsterdam = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=344";
	 } else {
	$query_amsterdam = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=68";
	 }
	$db =& JFactory::getDBO();
	$db->setQuery($query_amsterdam);
	$res_amsterdam = json_decode($db->loadResult(),true);

	 if ($lang == 'en-GB') {
	$query_alphavit = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=163";
	} else if ($lang == 'fr-FR') {
	$query_alphavit = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=261";
	} else if ($lang == 'de-DE') {
	$query_alphavit = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=352";
	 } else {
	$query_alphavit = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=76";
	 }
	$db =& JFactory::getDBO();
	$db->setQuery($query_alphavit);
	$res_alphavit = json_decode($db->loadResult(),true);

	 if ($lang == 'en-GB') {
	$query_regina = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=172";
	} else if ($lang == 'fr-FR') {
	$query_regina = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=270";
	} else if ($lang == 'de-DE') {
	$query_regina = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=361";
	 } else {
	$query_regina = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=85";
	 }
	$db =& JFactory::getDBO();
	$db->setQuery($query_regina);
	$res_regina = json_decode($db->loadResult(),true);

	 if ($lang == 'en-GB') {
	$query_eurasia = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=180";
	} else if ($lang == 'fr-FR') {
	$query_eurasia = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=278";
	} else if ($lang == 'de-DE') {
	$query_eurasia = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=368";
	 } else {
	$query_eurasia = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=3";
	 }
	$db =& JFactory::getDBO();
	$db->setQuery($query_eurasia);
	$res_eurasia = json_decode($db->loadResult(),true);

	 if ($lang == 'en-GB') {
	$query_vintage = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=189";
	} else if ($lang == 'fr-FR') {
	$query_vintage = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=287";
	} else if ($lang == 'de-DE') {
	$query_vintage = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=378";
	 } else {
	$query_vintage = "SELECT extra_fields FROM `azqy5_k2_items` WHERE id=94";
	 }
	$db =& JFactory::getDBO();
	$db->setQuery($query_vintage);
	$res_vintage = json_decode($db->loadResult(),true);

	?>

    <div class="b-map" id="map_canvas" style="margin:0;"></div>
<script>
    $("#map_canvas").width(screen.width * 0.8 - 20);
    $("#map_canvas").height(screen.height * 0.8);
</script>
    <script src="https://api-maps.yandex.ru/2.0/?load=package.full&lang=<? echo $lang_z; ?>" type="text/javascript"></script>
    <script>

        var myMap;
        ymaps.ready(init);
        function init() {

            var mZoom = 10;
            if ($("#map_canvas").width() > 1000) {
                mZoom = 11;
            }
            myMap = new ymaps.Map("map_canvas", {
                center: [59.92672, 30.3419],
                zoom: mZoom,
                behaviors: ["scrollZoom", "drag", "multiTouch"]
            });

            // Отключает открытие хинта при наведении мыши и открывает вместо него балун.

            myMap.geoObjects.options.set({
                showHintOnHover: false
            });

            myMap.geoObjects.events.add('mouseenter', function (e) {
                var geoObject = e.get('target'),
                    position = e.get('globalPixelPosition'),
                    balloon = geoObject.balloon.open(position);

                balloon.events.add('mouseleave', function () {
                    balloon.close();
                });
            });


            var myPlacemark0 = new ymaps.Placemark([<?php echo $res_dynasty[0][value]; ?>], {
                balloonContent: '<div class="b-map-info__content"> <?php  echo str_replace("'", "", $res_dynasty[1][value]); ?> <img style="cursor:pointer;position: absolute;top: 6px;right: -8px;width: 20px;height: 20px;" class="close" onclick="myMap.balloon.close()" src="/images/balloonClose.png"/></div>'
            }, {
                iconImageHref: '/images/marker.png',
                iconImageSize: [23, 38],
                iconImageOffset: [-12, -38],
                balloonContentSize: [400, 100],
                balloonLayout: "default#imageWithContent",
                balloonImageOffset: [-209, -116],
                balloonShadow: false,
                balloonAutoPan: false
            });

            myMap.geoObjects
                .add(myPlacemark0);


            var myPlacemark1 = new ymaps.Placemark([<?php echo $res_privilegia[0][value]; ?>], {
                balloonContent: '<div class="b-map-info__content"> <?php  echo str_replace("'", "", $res_privilegia[1][value]); ?> <img style="cursor:pointer;position: absolute;top: 6px;right: -8px;width: 20px;height: 20px;" class="close" onclick="myMap.balloon.close()" src="/images/balloonClose.png"/></div>'
            }, {
                iconImageHref: '/images/marker.png',
                iconImageSize: [23, 38],
                iconImageOffset: [-12, -38],
                balloonContentSize: [400, 100],
                balloonLayout: "default#imageWithContent",
                balloonImageOffset: [-209, -116],
                balloonShadow: false,
                balloonAutoPan: false
            });

            myMap.geoObjects
                .add(myPlacemark1);


            var myPlacemark2 = new ymaps.Placemark([<?php echo $res_amsterdam[0][value]; ?>], {
                balloonContent: '<div class="b-map-info__content"> <?php  echo str_replace("'", "", $res_amsterdam[1][value]); ?> <img style="cursor:pointer;position: absolute;top: 6px;right: -8px;width: 20px;height: 20px;" class="close" onclick="myMap.balloon.close()" src="/images/balloonClose.png"/></div>'
            }, {
                iconImageHref: '/images/marker.png',
                iconImageSize: [23, 38],
                iconImageOffset: [-12, -38],
                balloonContentSize: [400, 100],
                balloonLayout: "default#imageWithContent",
                balloonImageOffset: [-209, -116],
                balloonShadow: false,
                balloonAutoPan: false
            });

            myMap.geoObjects
                .add(myPlacemark2);


            var myPlacemark3 = new ymaps.Placemark([<?php echo $res_alphavit[0][value]; ?>], {
                balloonContent: '<div class="b-map-info__content"> <?php  echo str_replace("'", "", $res_alphavit[1][value]); ?> <img style="cursor:pointer;position: absolute;top: 6px;right: -8px;width: 20px;height: 20px;" class="close" onclick="myMap.balloon.close()" src="/images/balloonClose.png"/></div>'
            }, {
                iconImageHref: '/images/marker.png',
                iconImageSize: [23, 38],
                iconImageOffset: [-12, -38],
                balloonContentSize: [400, 100],
                balloonLayout: "default#imageWithContent",
                balloonImageOffset: [-209, -116],
                balloonShadow: false,
                balloonAutoPan: false
            });

            myMap.geoObjects
                .add(myPlacemark3);


            var myPlacemark4 = new ymaps.Placemark([<?php echo $res_regina[0][value]; ?>], {
                balloonContent: '<div class="b-map-info__content"> <?php  echo str_replace("'", "", $res_regina[1][value]); ?> <img style="cursor:pointer;position: absolute;top: 6px;right: -8px;width: 20px;height: 20px;" class="close" onclick="myMap.balloon.close()" src="/images/balloonClose.png"/></div>'
            }, {
                iconImageHref: '/images/marker.png',
                iconImageSize: [23, 38],
                iconImageOffset: [-12, -38],
                balloonContentSize: [400, 100],
                balloonLayout: "default#imageWithContent",
                balloonImageOffset: [-209, -116],
                balloonShadow: false,
                balloonAutoPan: false
            });

            myMap.geoObjects
                .add(myPlacemark4);

/*
            var myPlacemark5 = new ymaps.Placemark([<?php echo $res_eurasia[0][value]; ?>], {
                balloonContent: '<div class="b-map-info__content"> <?php  echo str_replace("'", "", $res_eurasia[1][value]); ?> <img style="cursor:pointer;position: absolute;top: 6px;right: -8px;width: 20px;height: 20px;" class="close" onclick="myMap.balloon.close()" src="/images/balloonClose.png"/></div>'
            }, {
                iconImageHref: '/images/marker.png',
                iconImageSize: [23, 38],
                iconImageOffset: [-12, -38],
                balloonContentSize: [400, 100],
                balloonLayout: "default#imageWithContent",
                balloonImageOffset: [-209, -116],
                balloonShadow: false,
                balloonAutoPan: false
            });

            myMap.geoObjects
                .add(myPlacemark5);
*/

            var myPlacemark6 = new ymaps.Placemark([<?php echo $res_vintage[0][value]; ?>], {
                balloonContent: '<div class="b-map-info__content"> <?php  echo str_replace("'", "", $res_vintage[1][value]); ?> <img style="cursor:pointer;position: absolute;top: 6px;right: -8px;width: 20px;height: 20px;" class="close" onclick="myMap.balloon.close()" src="/images/balloonClose.png"/></div>'
            }, {
                iconImageHref: '/images/marker.png',
                iconImageSize: [23, 38],
                iconImageOffset: [-12, -38],
                balloonContentSize: [400, 100],
                balloonLayout: "default#imageWithContent",
                balloonImageOffset: [-209, -116],
                balloonShadow: false,
                balloonAutoPan: false
            });

            myMap.geoObjects
                .add(myPlacemark6);


        }
    </script>


	</body>
	<?php } else if (($task == 'edit') or ($task == 'add')) { ?>
	<body class="contentpane edit_k2">
	<jdoc:include type="message" />
	<jdoc:include type="component" />
	</body>

	<?php } else { ?>
	<body style="padding:0; margin:0;">
	<jdoc:include type="message" />
	<jdoc:include type="component" />
	</body>
	<?php } ?>

</html>
