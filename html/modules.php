<?php
/**
 * @package     Joomla.Site
 * @subpackage  Template.system
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/*
 * none (output raw module content)
 */

function modChrome_title($module, &$params, &$attribs)
{ ?>
<?php if ($params->get('moduleclass_sfx') != '') { ?>
		<div class="<?php echo htmlspecialchars($params->get('moduleclass_sfx')); ?>">
<?php } else { ?>		
	
	<?php } ?>		
						<?php if ((bool) $module->showtitle) : ?>
							<p class="title"><?php echo $module->title; ?></p>
						<?php endif; ?>
					<?php echo $module->content; ?>
					
<?php if ($params->get('moduleclass_sfx') != '') { ?>
</div>
<?php } else { ?>		
		
	<?php } ?>						
		
		
		
		
	<?php
}


function modChrome_xhtml($module, &$params, &$attribs)
{
echo $module->content;
} 