<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
//print_r ($this->item->id);
$lang = JRequest::getVar('lang', null); 

?>
			<?php foreach ($this->item->extra_fields as $key=>$extraField): ?>
			<?php if($extraField->id == '39'): ?>
				<?php $intro_text = $extraField->value; ?>
			<?php endif; ?>
			<?php if($extraField->id == '41'): ?>
				<?php $but = $extraField->value; ?>
			<?php endif; ?>
			<?php endforeach; ?>




                            <div class="w-shadow special-offer-img">
                                            <i class="b-shadow"></i>
						<img alt="<?php echo $this->item->title; ?>" src="<?php echo $this->item->image; ?>" class="b-img">
                                    </div>
                <div class="b-clear"></div>
               			<?php if(isset($this->item->editLink)): ?>
			<!-- Item edit link -->
			<div class="catItemEditLink" style="margin-top:5px;">
				<a class="modal" rel="{handler:'iframe',size:{x:990,y:610}}" href="<?php echo $this->item->editLink; ?>">
					<?php echo JText::_('K2_EDIT_ITEM'); ?>
				</a>
			</div>
			<?php endif; ?>

            <h2 class="b-category__header">
                                    <?php echo $this->item->title; ?>                          </h2>
                            <h3 class="b-category__subheader"><?php echo $intro_text; ?></h3>
                        <div class="slogan-text"><?php echo $this->item->introtext; ?> <a class="more-link" href="javascript:void(0);">
						<?php if ($lang == 'en-GB') { ?>
						Details
						<?php } else if ($lang == 'fr-FR') { ?>
						Details
						<?php } else if ($lang == 'de-DE') { ?>
						Details
						<?php } else { ?>
						Подробнее
						<?php } ?>
						</a></div>

            <div id="<?php echo $this->item->id; ?>" class="preview">
	<?php echo $this->item->fulltext; ?>


<div class="b-more-button">
  <?php echo $but; ?>
</div>


                <a class="hide-link" href="javascript:void(0);">	
				<?php if ($lang == 'en-GB') { ?>
						Hide
						<?php } else if ($lang == 'fr-FR') { ?>
						Hide
						<?php } else if ($lang == 'de-DE') { ?>
						Hide
						<?php } else { ?>
						Скрыть
						<?php } ?>
</a>
            </div>


