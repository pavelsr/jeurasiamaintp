<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
//print_r ($this->item);
?>
			<?php foreach ($this->item->extra_fields as $key=>$extraField): ?>
			<?php if($extraField->id == '36'): ?>
				<?php $links = $extraField->value; ?>
			<?php endif; ?>
			<?php if($extraField->id == '37'): ?>
				<?php $descr = $extraField->value; ?>
			<?php endif; ?>
			<?php if($extraField->id == '38'): ?>
				<?php $hot_id = $extraField->value; ?>
			<?php endif; ?>
			
			<?php endforeach; ?>

			<div class="b-shadow-borders b-shadow-borders_side_left"></div>
			<div class="b-shadow-borders b-shadow-borders_side_right"></div>

			<div class="b-category__left">
								<div class="w-shadow">
					<a title="<?php echo $this->item->title; ?>" href="/<?php echo $links; ?>">
						<i class="b-shadow"></i>
												<img alt="<?php echo $this->item->image_caption; ?>" src="<?php echo $this->item->image; ?>" class="b-img">
					</a>
				</div>

	<div class="b-hotel-info">
	<?php echo $this->item->fulltext; ?>
</div>	
</div>

			<div class="b-category__right">
				<h2 class="b-category__header">
					<a title="<?php echo $this->item->title; ?>" href="/<?php echo $links; ?>" class="b-link"><?php echo $this->item->title; ?></a>
				</h2>
				<div class="b-reservation-all__column-left">
	<?php echo $this->item->introtext; ?>			
				</div>        
				<div class="b-clear"></div>

			<div class="all_descr<?php echo $hot_id; ?>">
			<?php echo $descr; ?>
			</div>

			
			</div>
		