<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
//print_r ($this->item);
?>


<a title="<?php echo $this->item->title; ?>" href="<?php echo $this->item->link; ?>" class="link">
                    <span class="name"><?php echo $this->item->title; ?></span>
                                        <img alt="<?php echo $this->item->title; ?>" src="<?php echo $this->item->image; ?>" class="photo">
</a>