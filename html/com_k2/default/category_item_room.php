<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
//print_r ($this->item);
$lang = JRequest::getVar('lang', null); 
?>


		
	  <?php if($this->item->params->get('catItemTitle')): ?>
	  <!-- Item title -->
	 <h2 class="title_rooms">
			<?php if(isset($this->item->editLink)): ?>
			<!-- Item edit link -->
			<?php endif; ?>

	  	<?php if ($this->item->params->get('catItemTitleLinked')): ?>
			<a class="b-link" title="<?php echo $this->item->title; ?>" href="<?php echo $this->item->link; ?>">
	  		<span><?php echo $this->item->title; ?></span>
	  	</a>
	  	<?php else: ?>
	  	<?php echo $this->item->title; ?>
	  	<?php endif; ?>
	  </h2>
	  <?php endif; ?>		


			<?php echo $this->item->introtext; ?>    

			
        <div class="b-clear"></div>
        <div class="b-rooms-item__divider"></div>
