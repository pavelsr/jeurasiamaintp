<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
//print_r ($this->item);
$lang = JRequest::getVar('lang', null); 

?>


		<span class="news-date-time">	
			<?php echo JHTML::_('date', $this->item->created , 'd.m.y'); ?>
		</span>
		
	  <?php if($this->item->params->get('catItemTitle')): ?>
	  <!-- Item title -->
	 <h2 class="b-category__header">
			<?php if(isset($this->item->editLink)): ?>
			<!-- Item edit link -->
			<?php endif; ?>

	  	<?php if ($this->item->params->get('catItemTitleLinked')): ?>
					<?php if ($this->item->fulltext != '') { ?>  
			<a class="b-link" title="<?php echo $this->item->title; ?>" href="<?php echo $this->item->link; ?>">
					<?php } ?>
	  		<span><?php echo $this->item->title; ?></span>
					<?php if ($this->item->fulltext != '') { ?>  
	  	</a>
					<?php } ?>
	  	<?php else: ?>
	  	<?php echo $this->item->title; ?>
	  	<?php endif; ?>
	  </h2>
	  <?php endif; ?>		

 <?php if($this->item->params->get('catItemImage') && !empty($this->item->image)): ?>
	  <div class="b-category__left">
                           <div class="w-shadow">
						   <?php if ($this->item->fulltext != '') { ?>  
                                <a title="<?php echo $this->item->title; ?>" href="<?php echo $this->item->link; ?>">
						   <?php } ?>
                                    <i class="b-shadow"></i>
				    	<img src="<?php echo $this->item->image; ?>" alt="<?php echo $this->item->title; ?>" width="92px" class="b-img" />
							<?php if ($this->item->fulltext != '') { ?>  
                                </a>
								  <?php } ?>
                            </div>
                                </div>
				<?php endif; ?>		
				
                            <div class="b-category__right">
			<?php echo $this->item->introtext; ?>         
<?
 if ($lang == 'en-GB') { 
$more_text = 'More';
 } else if ($lang == 'fr-FR') {
$more_text = 'Détails';
 } else if ($lang == 'de-DE') { 
$more_text = 'Mehr';
 } else { 
$more_text = 'Подробнее';
 } 	
 ?>
			
			<?php if ($this->item->fulltext != '') { ?>  
                            <div class="b-more-button">
                            <a href="<?php echo $this->item->link; ?>" class="b-more-button__link"><span class="b-more-button__span"><?php echo $more_text; ?></span></a>
							</div>
			<?php } ?>  
                            </div>
         	
		                 
