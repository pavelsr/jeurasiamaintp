<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
$tmpl = JRequest::getVar('tmpl', null); 
//echo $tmpl;
$menu = JFactory::getApplication()->getMenu();
$active_z = $menu->getActive();
//echo $active_z->params->get('pageclass_sfx');
$lang = JRequest::getVar('lang', null); 

$menuname = $active_z->title;
if ($active_z->tree[2]) {
$parentId = $active_z->tree[2];
$parent1 = $menu->getItem($parentId);
if ($parent1->params->get('pageclass_sfx') == 'details_room') {
	$suff1 = 'details_room';
}
} 


?>
<?php if ($tmpl == 'component') { ?>
<?php foreach ($this->item->extra_fields as $key=>$extraField):  
if ($extraField->id == '35') {  
$lat = $extraField->value; 
}
endforeach;
 ?>
 <style>
 #map_canvas {width:735px;height:449px;}
 @media (max-width: 700px) {
 #map_canvas {width:335px;height:249px;}	 
 }
 
 </style>
	<div id="map_canvas" class="w-hotelmap"></div>
	<?php if ($lang == 'en-GB') { 
	$lang_z = 'en-US'; 
	} else if ($lang == 'fr-FR') {
	// $lang_z = 'fr-FR'; yandex не поддерживает (pkl)
	$lang_z = 'en-US'; 
	} else if ($lang == 'de-DE') {
	$lang_z = 'de-DE'; 
	} else {
	$lang_z = 'ru-RU';
	}
	?>
  <script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="https://api-maps.yandex.ru/2.0/?load=package.full&amp;lang=<?php echo $lang_z; ?>"></script>
<script type="text/javascript">$(document).ready(function () {
 var myMap;
        ymaps.ready(init);
        function init() {

            var mZoom = 14;

            myMap = new ymaps.Map("map_canvas", {
                center: [<?php echo $lat; ?>],
                zoom: mZoom,
                behaviors: ["scrollZoom", "drag"]
            });


            var myPlacemark = new ymaps.Placemark([<?php echo $lat; ?>], {}, {
                iconImageHref: '/images/marker.png',
                iconImageSize: [23, 38],
                iconImageOffset: [-12, -38]
            });

            myMap.geoObjects
                .add(myPlacemark);
        }    });</script>

		
<?php } else { ?>

   			<?php if(isset($this->item->editLink)): ?>
			<!-- Item edit link -->
			<span class="itemEditLink">
				<a class="modal" rel="{handler:'iframe',size:{x:990,y:610}}" href="<?php echo $this->item->editLink; ?>">
					<?php echo JText::_('K2_EDIT_ITEM'); ?>
				</a>
			</span>
			<?php endif; ?>

<?php if($this->item->params->get('pageclass_sfx') == 'mainpage') { ?>	
<?php foreach ($this->item->extra_fields as $key=>$extraField):  
if ($extraField->id == '42') {  
$hotels_block = $extraField->value; 
}
if ($extraField->id == '43') {  
$right_size = $extraField->value; 
}
endforeach; ?>

   <h1>
	  	<?php echo $this->item->title; ?>
   </h1>
  <div class="b-divider"></div>
 <?php echo $this->item->introtext; ?>          
	
	<div class="b-hotel-group clearfix">
  <?php echo $hotels_block; ?>
	</div>

	<div class="clear_z"></div>	
	<div class="main_content_bottom">
	<?php if ($this->item->fulltext != '') { ?>
<div class="b-a2b">
            <div class="main-content">	
<?php echo $this->item->fulltext; ?>
</div>       
 </div>
	<?php } ?>
 
<?php if (false) { 
// Отключил pkl
?>  
<div class="right_main_cont">

 <?php echo $right_size; ?>
<?php } ?>
<?php if (false) { 
// Отключил pkl
?>  
<div class="b-news_page_main">
                                <div class="b-shadow-borders"></div>
                    <h2 class="b-header_level_2">
					<?php if ($lang == 'en-GB') { ?>
					<a title="News" href="en/news/" class="b-link">News</a>
					<?php 	} else if ($lang == 'fr-FR') { ?>
					<a title="Nouvelles" href="fr/news/" class="b-link">Nouvelles</a>
					<?php 	} else if ($lang == 'de-DE') { ?>
					<a title="Nouvelles" href="de/news/" class="b-link">Nachrichten</a>
					<?php } else { ?>
					<a title="Новости" href="news/" class="b-link">Новости</a>
					<?php } ?>
					</h2>
                    <div class="b-divider"></div>
<?php } ?>
  <?php
  $document   = & JFactory::getDocument();
$renderer   = $document->loadRenderer('modules');
$options    = array('style' => 'title');
$position   = 'news';
echo $renderer->render($position, $options, null);
?>                 
<?php if (false) { 
// Отключил pkl
?>  
<div class="b-more-button">

					<?php if ($lang == 'en-GB') { ?>
<a title="All news" href="en/news/" class="b-more-button__link"><span class="b-more-button__span">All news</span></a>
					<?php 	} else if ($lang == 'fr-FR') { ?>
<a title="Toutes les nouvelles" href="fr/news/" class="b-more-button__link"><span class="b-more-button__span">Toutes les nouvelles</span></a>
					<?php 	} else if ($lang == 'de-DE') { ?>
<a title="Weitere nachrichten" href="de/news/" class="b-more-button__link"><span class="b-more-button__span">Weitere nachrichten</span></a>
					<?php } else { ?>
<a title="Все новости" href="news/" class="b-more-button__link"><span class="b-more-button__span">Все новости</span>
</a>
					
					<?php } ?>
</div>
        </div>
<?php } ?>
        
<?php if (false) { 
// Убрал карту pkl
?>  
  <div class="b-official-groups">
    <div class="b-shadow-borders"></div>    
    <h2 class="b-header_level_2">
					<?php if ($lang == 'en-GB') { ?>
	<a class="big-map-colorbox" title="Hotels on the map" href="<?php echo JRoute::_('index.php?option=com_content&view=article&id=9&lang=en&tmpl=component'); ?>">Hotels on the map </a>
					<?php 	} else if ($lang == 'fr-FR') { ?>
	<a class="big-map-colorbox" title="Hôtels sur la carte" href="<?php echo JRoute::_('index.php?option=com_content&view=article&id=9&lang=fr&tmpl=component'); ?>">Hôtels sur la carte</a>
					<?php 	} else if ($lang == 'de-DE') { ?>
	<a class="big-map-colorbox" title="Hotels auf der Karte " href="<?php echo JRoute::_('index.php?option=com_content&view=article&id=9&lang=de&tmpl=component'); ?>">Hotels auf der Karte </a>
					<?php } else { ?>
	<a class="big-map-colorbox" title="Отели на карте" href="<?php echo JRoute::_('index.php?option=com_content&view=article&id=9&tmpl=component'); ?>">Отели на карте</a>
					<?php } ?>
    </h2>
    <div class="b-divider"></div>
    <div class="links">
	
					<?php if ($lang == 'en-GB') { ?>
      <a style="width: 219px;height: 130px;margin: -14px 0px 0px -20px;display: block;background: #FFF;padding: 10px 3px 3px 3px;" class="big-map-colorbox" title="Hotels on the map" href="<?php echo JRoute::_('index.php?option=com_content&view=article&id=9&lang=en&tmpl=component'); ?>">
	<img alt="map" src="/images/en-map-preview.bmp">
      </a>
					<?php 	} else if ($lang == 'fr-FR') { ?>
      <a style="width: 219px;height: 130px;margin: -14px 0px 0px -20px;display: block;background: #FFF;padding: 10px 3px 3px 3px;" class="big-map-colorbox" title="Hotels on the map" href="<?php echo JRoute::_('index.php?option=com_content&view=article&id=9&lang=fr&tmpl=component'); ?>">
	<img alt="map" src="/images/en-map-preview.bmp">
      </a>
					<?php 	} else if ($lang == 'de-DE') { ?>
      <a style="width: 219px;height: 130px;margin: -14px 0px 0px -20px;display: block;background: #FFF;padding: 10px 3px 3px 3px;" class="big-map-colorbox" title="Hotels on the map" href="<?php echo JRoute::_('index.php?option=com_content&view=article&id=9&lang=de&tmpl=component'); ?>">
	<img alt="map" src="/images/en-map-preview.bmp">
      </a>
				
					<?php } else { ?>
      <a style="width: 219px;height: 130px;margin: -14px 0px 0px -20px;display: block;background: #FFF;padding: 10px 3px 3px 3px;" class="big-map-colorbox" title="Отели на карте" href="<?php echo JRoute::_('index.php?option=com_content&view=article&id=9&tmpl=component'); ?>">
	<img alt="map" src="/images/map-preview.bmp">
      </a>
					<?php } ?>
	  
          </div>

</div>
<?php } ?>

<?php if (false) { 
// Отключил pkl
?>  
    <div class="b-official-groups soc_link_mobile">
    <h2 class="b-header_level_2">
					<?php if ($lang == 'en-GB') { ?>
We are in social networks
					<?php 	} else if ($lang == 'fr-FR') { ?>
Nous sommes dans les réseaux sociaux
					<?php 	} else if ($lang == 'de-DE') { ?>
Wir sind in sozialen Netzwerken
					<?php } else { ?>
Мы в социальных сетях
					<?php } ?>
    </h2>
    <div class="b-divider"></div>
		<div class="soc_link"> 
 	     <?php echo $document->getBuffer('modules', 'social_links', array('style' => 'none')); ?>
		</div>
    </div>    



</div>
<?php } ?>


<?php if (false) { 
// Отключил pkl
?>
	</div>
<?php } ?>
		
	<div class="clear_z"></div>	
	
 
<?php } else { ?>
<?php if($this->item->params->get('pageclass_sfx')) : ?>
<div class="page_<?php echo ($this->item->featured) ? ' itemIsFeatured' : ''; ?><?php if($this->item->params->get('pageclass_sfx')) echo ''.$this->item->params->get('pageclass_sfx'); ?>">
<?php endif; ?>

	<?php echo $this->item->event->BeforeDisplay; ?>

	<?php echo $this->item->event->K2BeforeDisplay; ?>
	  <?php if($this->item->params->get('itemTitle')): ?>
	  <h1>
	  	<?php echo $this->item->title; ?>
	  </h1>
	  <?php endif; ?>

	  	<?php if (($this->item->params->get('pageclass_sfx') != 'hotels-group') and ($this->item->catid != '36') and ($this->item->catid != '37') and ($this->item->catid != '10') and ($this->item->catid != '69')) { ?>
		<div class="b-divider"></div>
	<?php } ?>
	 

<?php if (($this->item->params->get('pageclass_sfx') == 'cafe') or ($this->item->params->get('pageclass_sfx') == 'cafe hidden_l')) { ?>	

<div class="b-restaurant">
<div class="b-restaurant__left">
<div class="itemRelImageGallery"><?php echo $this->item->gallery; ?></div>
<div class="b-clear"></div>
<?php echo $this->item->fulltext; ?>
</div>
<div class="b-restaurant__right">

<?php echo $this->item->introtext; ?>
</div>
<?php foreach ($this->item->extra_fields as $key=>$extraField):  
if ($extraField->id == '34') {  
$descr = $extraField->value; 
}
endforeach;
 ?>
<?php if ($descr != '') { ?>
<div class="b-clear"></div>
<?php echo $descr; ?>
<?php } ?>
</div>
<?php } else if ($this->item->params->get('pageclass_sfx') == 'price_booking') { ?>	

<?php echo $this->item->introtext; ?>

<?php foreach ($this->item->extra_fields as $key=>$extraField):  
if ($extraField->id == '33') {  
echo $extraField->value; 
}
if ($extraField->id == '34') {  
echo $extraField->value; 
}
endforeach; ?>

<?php echo $this->item->fulltext; ?>

<?php } else if ($this->item->params->get('pageclass_sfx') == 'hotels-group') { ?>

<?php echo $this->item->gallery; ?>
<div class="w-text">
<?php echo $this->item->introtext; ?>
</div>

<?php } else if ($this->item->catid == '12') { ?>
<div class="news-list">
      <div class="b-category b-category_news_detail">
                <div class="b-category__item clearfix">
                    <div class="b-shadow-borders b-shadow-borders_side_left"></div>
                    <div class="b-shadow-borders b-shadow-borders_side_right"></div>
                    <div class="news-detail-img">
                                            </div>
<?php echo $this->item->fulltext; ?>

<?php echo $this->item->gallery; ?>

                </div>
            </div>
                                        
                    <div class="article_nav">

		<?php if(isset($this->item->previousLink)): ?>
		<div class="w-prev-news">
		<a class="itemPrevious" href="<?php echo $this->item->previousLink; ?>">
			&laquo; <?php echo $this->item->previousTitle; ?>
		</a>
		</div>
		<?php endif; ?>

		<?php if(isset($this->item->nextLink)): ?>
		<div class="w-next-news">
		<a class="itemNext" href="<?php echo $this->item->nextLink; ?>">
			<?php echo $this->item->nextTitle; ?> &raquo;
		</a>
		</div>
		<?php endif; ?>

					</div>
                    
                </div>
<?php } else if ($this->item->catid == '37') { ?>

<div class="beauty-container">
    <img width="707" title="<?php echo $this->item->title; ?>" alt="<?php echo $this->item->image_caption; ?>" src="<?php echo $this->item->image; ?>" class="beauty-image">

    <div class="clearfix beauty-controls">


		<?php if(isset($this->item->previousLink)): ?>
		<a href="<?php echo $this->item->previousLink; ?>" class="strbg prev-beauty active"></a>
		<?php else : ?>
		<span class="strbg prev-beauty"></span>
		<?php endif; ?>

		<?php if(isset($this->item->nextLink)): ?>
		<a href="<?php echo $this->item->nextLink; ?>" class="strbg next-beauty active"></a>
		<?php else : ?>
		<span class="strbg next-beauty"></span>
		<?php endif; ?>	
	
        <b><?php echo $this->item->title; ?></b> (<a download="" href="<?php echo $this->item->image; ?>">Скачать изображение</a>)

    </div>
</div>		
<div class="beauty-detail">
		  <?php if(!empty($this->item->fulltext)): ?>
	  <?php if($this->item->params->get('itemIntroText')): ?>
	  <!-- Item introtext -->
	  
	  	<?php echo $this->item->introtext; ?>
	  <?php endif; ?>
	  <?php if($this->item->params->get('itemFullText')): ?>
	  	<?php echo $this->item->fulltext; ?>
	  <?php endif; ?>
	  <?php else: ?>
	  	<?php echo $this->item->introtext; ?>
	  <?php endif; ?>
</div>		
<?php } else if (($this->item->params->get('pageclass_sfx') == 'details_room') or ($suff1 == 'details_room')) { ?>
<div class="page_details_room">
<?php echo $this->item->fulltext; ?>

<?php foreach ($this->item->extra_fields as $key=>$extraField): 
if ($extraField->id == '33') {  
echo $extraField->value; 
}
if ($extraField->id == '34') {  
echo $extraField->value; 
}
endforeach; ?>
</div>

<?php } else { ?>	  
	
	  <?php if(!empty($this->item->fulltext)): ?>
	  <?php if($this->item->params->get('itemIntroText')): ?>
	  <!-- Item introtext -->
	  
	  	<?php echo $this->item->introtext; ?>
	  <?php endif; ?>
	  <?php if($this->item->params->get('itemFullText')): ?>
	  	<?php echo $this->item->fulltext; ?>
	  <?php endif; ?>
	  <?php else: ?>
	  	<?php echo $this->item->introtext; ?>
	  <?php endif; ?>
	  
  <?php if($this->item->params->get('itemComments') && ( ($this->item->params->get('comments') == '2' && !$this->user->guest) || ($this->item->params->get('comments') == '1'))): ?>
  <?php echo $this->item->event->K2CommentsBlock; ?>
  <?php endif; ?>
	  
	  
<?php } ?>

<?php if(($active_z->params->get('pageclass_sfx') == 'hotel_descr') or ($this->item->params->get('pageclass_sfx') == 'cafe') or ($this->item->params->get('pageclass_sfx') == 'cafe hidden_l')) { ?>
<div class="clear_z"></div>
  <?php
  $document   = & JFactory::getDocument();
$renderer   = $document->loadRenderer('modules');
$options    = array('style' => 'title');
$position   = 'offer_hotel_bottom';
echo $renderer->render($position, $options, null);
?>    

<?php } ?>


<?php if($this->item->params->get('pageclass_sfx')) : ?>	
</div>
 <?php endif; ?>
<?php } ?>
<?php } ?>
