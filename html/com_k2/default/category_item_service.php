<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
//print_r ($this->item);
$lang = JRequest::getVar('lang', null); 

?>
    <div class="b-shadow-borders b-shadow-borders_side_left"></div>
    <div class="b-shadow-borders b-shadow-borders_side_right"></div>
    <h2 class="b-header_level_2">
<?php echo $this->item->title; ?>

</h2>
    <div class="b-divider"></div>

      <div class="w-shadow">
        <a href="<?php echo $this->item->image_credits; ?>" title="<?php echo $this->item->image_caption; ?>">
          <i class="b-shadow"></i>
          <img class="b-img" src="<?php echo $this->item->image; ?>" alt="<?php echo $this->item->image_caption; ?>"/>
        </a>
      </div>

    <div class="b-service-list__text">
 	<?php echo $this->item->introtext; ?>

	<div class="b-more-button">
	<?php if ($lang == 'en-GB') {
	$more = 'More';
	} else if ($lang == 'fr-FR') {
	$more = 'Plus d’informations';
	} else if ($lang == 'de-DE') {
	$more = 'Mehr lesen';
	} else {
	$more = 'Подробнее';
	}
	?>
        <a class="b-more-button__link" href="<?php echo $this->item->image_credits; ?>" title="<? echo $more; ?>"><span class="b-more-button__span"><? echo $more; ?></span></a>
      </div>
    </div>
    <div class="b-service-list__hotel alexander">
<?php echo $this->item->fulltext; ?>    </div>

    <div class="b-clear"></div>