<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);
//print_r ($this->item->id);
?>
              <div class="w-shadow">
			  <?php if ($this->item->image_caption != '') { ?>
                    <a target="_blank" title="<?php echo $this->item->title; ?>" rel="nofollow" href="http://<?php echo $this->item->image_caption; ?>">
			  <?php } ?>		
                        <i class="b-shadow"></i>
                            <img alt="<?php echo $this->item->title; ?>" src="<?php echo $this->item->image; ?>" class="b-img">
				<?php if ($this->item->image_caption != '') { ?>			</a>
				<?php } ?>
                </div>
                <div class="b-clear"></div>

            <h2 class="b-category__header"><?php echo $this->item->title; ?></h2>
<?php echo $this->item->introtext; ?>
<?php if ($this->item->image_caption != '') { ?>
<noindex>
    <a target="_blank" rel="nofollow" href="http://<?php echo $this->item->image_caption; ?>" class="partners-list__link"><?php echo $this->item->image_caption; ?></a>
</noindex>
<?php } ?>
