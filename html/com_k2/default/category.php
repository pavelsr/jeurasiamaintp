<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
$pageClassSuffix = JSite::getMenu()->getActive()? JSite::getMenu()->getActive()->params->get('pageclass_sfx', '-default') : '-default';
?>
	<?php if($this->params->get('show_page_title')): ?>
	<h1 class="b-header_level_1">
		<?php echo $this->escape($this->params->get('page_title')); ?>
	</h1>
	
	<?php else: ?>
<h1 class="b-header_level_1">
<?php echo $this->category->name; ?>
</h1>	
	<?php endif; ?>

<?php if ($this->category->id == '9') { ?>

<ul class="b-service-list">


	<?php foreach($this->leading as $key=>$item): ?>
  <li class="b-service-list-item">
				<?php
					// Load category_item.php by default
					$this->item=$item;
					echo $this->loadTemplate('item_service');
				?>
				</li>
			<?php endforeach; ?>
  


</ul>
<?php } else if ($this->category->id == '10') { ?>

<div class="b-category b-category_type_specoffers clearfix">

	<?php foreach($this->leading as $key=>$item):
	$z++;
	?>
 <div class="b-category__item">
				<?php
					// Load category_item.php by default
					$this->item=$item;
					echo $this->loadTemplate('item_offers');
				?>
</div>
<?php if ($z%3 == 0) { ?>
<div class="b-clear"></div>
<?php } ?>
			<?php endforeach; ?>
  


</div>



<script type="text/javascript">
    function parseGetParams() { //получить параметры GET запроса
       var $_GET = {}; 
       var __GET = window.location.search.substring(1).split("&"); 
       for(var i=0; i<__GET.length; i++) { 
          var getVar = __GET[i].split("="); 
          $_GET[getVar[0]] = typeof(getVar[1])=="undefined" ? "" : getVar[1]; 
       } 
       return $_GET; 
    }
    (function () {
		//$("html").animate({ scrollTop: 900 }, "slow");
		})();
    window.onload = function () {

        var getStr = parseGetParams();
        var activeItem = getStr["ID"];
        if (activeItem != "undefined" && activeItem != 10000 && activeItem != 100001) {
            $("#" + activeItem).toggle( "slow", function() {});
            $("#" + activeItem).parent().children("div").children(".more-link").toggle( "slow", function() {
				//$("html").animate({ scrollTop: 900 }, "slow");
                $("html,body").animate({
                    scrollTop: $("#" + activeItem).parent().offset().top
                }, "slow");
            });
        }
        else {
			
        }     
        $(".slogan-text, .special-offer-img").click(function () {
            //показать текс и скрыть ссылку
            $(this).parent().children(".preview").toggle( "slow", function() {});
            $(this).parent().children("div").children(".more-link").toggle( "slow", function() {});
        });
        $(".hide-link").click(function () {
            $(this).parent().toggle( "slow", function() {});
            $(this).parent().parent().children("div").children(".more-link").toggle( "slow", function() {});
        });
    }
</script>﻿



<?php } else if (($this->category->id == '7') or ($this->category->id == '38') or ($this->category->id == '39') or ($this->category->id == '40')) { ?>

<div class="b-category">

<?php if ($this->category->id == '7') { ?>
<div class="b-category__item b-reservation-all__item clearfix">
			<span class="b-reservation-all__text">Выбор отеля для бронирования</span>
			<select class="b-reservation-all__select clearfix" name="hotels">
				<option value="-1" selected="selected">Все отели</option>
								<option value="174">
					Династия				</option>
								<option value="176">
					Амстердам				</option>
								<option value="1726">
					Алфавит				</option>
								<option value="177">
					Регина				</option>
								<option value="911">
					Винтаж				</option>
								<option value="6022">
					Привилегия				</option>
							</select>
		</div>
<?php } else if ($this->category->id == '38') { ?>
<div class="b-category__item b-reservation-all__item clearfix">
			<span class="b-reservation-all__text">Choice of hotel</span>
			<select name="hotels" class="b-reservation-all__select clearfix">
				<option selected="selected" value="-1">All hotels</option>
								<option value="174">
					Dynasty				</option>
								<option value="176">
					Amsterdam				</option>
								<option value="1726">
					Alfavit				</option>
								<option value="177">
					Regina				</option>
								<option value="911">
					Vintage				</option>
								<option value="6022">
					Privilegia			</option>
							</select>
		</div>

<?php } else if ($this->category->id == '39') { ?>
<div class="b-category__item b-reservation-all__item clearfix">
			<span class="b-reservation-all__text">Choisir un hôtel</span>
			<select name="hotels" class="b-reservation-all__select clearfix">
				<option selected="selected" value="-1">Tous les hôtels</option>
								<option value="174">
					Dynastia				</option>
								<option value="176">
					Amsterdam				</option>
								<option value="1726">
					Alfavit				</option>
								<option value="177">
					Régina				</option>
								<option value="911">
					Vintage				</option>
								<option value="6022">
					Privilégia			</option>
							</select>
		</div>

	<?php } else if ($this->category->id == '40') { ?>
<div class="b-category__item b-reservation-all__item clearfix">
<span class="b-reservation-all__text">Wählen Hotel</span>
			<select class="b-reservation-all__select clearfix" name="hotels">
				<option value="-1" selected="selected">Alle Hotels</option>
								<option value="174">
					Dynastie				</option>
								<option value="176">
					Amsterdam				</option>
								<option value="1726">
					Alfavit				</option>
								<option value="177">
					Regina				</option>
								<option value="911">
					Vintage				</option>
								<option value="6022">
					Privilegia			</option>
							</select>		</div>

<?php } ?>	

<?php echo $this->category->description; ?>

	<?php foreach($this->leading as $key=>$item): ?>
	<div class="b-category__item b-category__item_set_height clearfix">
				<?php
					// Load category_item.php by default
					$this->item=$item;
					echo $this->loadTemplate('item');
				?>
				</div>
			<?php endforeach; ?>


</div>
<?php } else if ($this->category->id == '34') { ?>
<div class="b-category b-category_type_partners clearfix">
<?php foreach($this->leading as $key=>$item): 
$z++; ?>

<div class="b-category__item">
				<?php
					$this->item=$item;
					echo $this->loadTemplate('item_partners');
				?>
	</div>
<?php if ($z%3 == 0) { ?>
<div class="b-clear"></div>
<?php } ?>
	
			<?php endforeach; ?>



</div>
<?php } else if ($this->category->id == '37') { ?>
<div class="catalog-section">
        <ul class="b-attractions">
		<?php foreach($this->leading as $key=>$item): ?>
<li class="item">
				<?php
					$this->item=$item;
					echo $this->loadTemplate('item_beautiful');
				?>
</li>
			<?php endforeach; ?>


</ul>
</div>
<?php } else if ($pageClassSuffix == 'details_room') { 
$descr_rooms = explode("<hr />", $this->category->description);
?>

<div class="b-divider"></div>
<div class="details_room_descr">
<?php echo $descr_rooms[0]; ?>
</div>
<div class="b-divider"></div>

<ul class="b-rooms-list">
<?php foreach($this->leading as $key=>$item): ?>

<li class="b-rooms-item">
				<?php
					$this->item=$item;
					echo $this->loadTemplate('item_room');
				?>
</li>
	
			<?php endforeach; ?>
</ul>

<?php echo $descr_rooms[1]; ?>

<?php } else { ?>

<div class="b-category b-category_type_news">

	<?php if($this->pagination->getPagesLinks()): ?>
	<div class="b-pagination b-pagination_hotel_group">
<span class="b-pagination__name">Страницы:</span>
		<?php if($this->params->get('catPagination')) echo $this->pagination->getPagesLinks(); ?>

	</div>
	<?php endif; ?>

<div class="w-category-items">
<?php foreach($this->leading as $key=>$item): ?>
<div class="b-category__item clearfix">
				<?php
					$this->item=$item;
					echo $this->loadTemplate('item_news');
				?>
				</div>
			<?php endforeach; ?>
</div>			
	<?php if($this->pagination->getPagesLinks()): ?>
	<div class="b-pagination b-pagination_hotel_group">
<span class="b-pagination__name">Страницы:</span>
		<?php if($this->params->get('catPagination')) echo $this->pagination->getPagesLinks(); ?>

	</div>
	<?php endif; ?>			
			
</div>


<?php } ?>
