<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
$lang = JRequest::getVar('lang', null); 

?>
 <?php foreach ($items as $key=>$item):	?>
	<?php if ($lang == 'en-GB') { 
	$lang_z = 'en-US'; 
	$close = 'Close';
	$map = 'Map';
	} else if ($lang == 'fr-FR') {
	// $lang_z = 'fr-FR'; Yandex не поддерживает (pkl)
	$lang_z = 'en-US'; 
	$close = 'Close';
	$map = 'Map';
	} else if ($lang == 'de-DE') {
	$lang_z = 'de-DE'; 
	$close = 'Close';
	$map = 'Map';
	} else {
	$lang_z = 'ru-RU';
	$close = 'Закрыть';
	$map = 'Карта';
	}
	?>
 <div class="b-hotelmap">
<div class="b-shadow-borders"></div>
<?php echo $item->introtext; ?>
<? $extra = (json_decode($item->extra_fields));	
if ($extra[0]->value != '') { ?>
<div id="map_canvas" class="w-hotelmap"></div>
<a href="index.php?option=com_k2&view=item&layout=item&id=<?php echo $item->id; ?>&tmpl=component" id="map-colorbox" class="map-detail" title="<?php echo $map; ?>"><span>Показать карту</span></a>
<script type="text/javascript" src="https://api-maps.yandex.ru/2.0/?load=package.full&amp;lang=<?php echo $lang_z; ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
	function windowSize(){
    if ($(window).width() <= '995'){
        $("#map-colorbox").colorbox({iframe: true, fastIframe: false, fixed:true,	scrolling:false, opacity:'1', scalePhotos:true,  width:'84%', height:'300px', maxWidth:'700px', close: '<?php echo $close; ?>'});
    } else {
        $("#map-colorbox").colorbox({iframe: true, fastIframe: false, width: "740px", height: "500px", close: '<?php echo $close; ?>'});
    }
}
$(window).on('load resize',windowSize);

        var myMap;
        ymaps.ready(init);
        function init() {

            var mZoom = 13;

            myMap = new ymaps.Map("map_canvas", {
                center: [<?php echo $extra[0]->value; ?>],
                zoom: mZoom,
                behaviors: ["scrollZoom", "drag"]
            });

            var myPlacemark = new ymaps.Placemark([<?php echo $extra[0]->value; ?>], {}, {
                iconImageHref: '/images/marker.png',
                iconImageSize: [23, 38],
                iconImageOffset: [-12, -38]
            });

            myMap.geoObjects
                .add(myPlacemark);

        }
    });</script>

	
	
<?}?>
</div>

<?
$document   = & JFactory::getDocument();
$renderer   = $document->loadRenderer('modules');
$options    = array('style' => 'title');
$position   = 'currancy';
echo $renderer->render($position, $options, null);
?>


<?php endforeach; ?>
