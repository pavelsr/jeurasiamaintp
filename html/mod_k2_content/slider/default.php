<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>
              <link rel="stylesheet" href="/js/nivo/themes/hotel_inner/default.css" type="text/css" media="screen">
    <div class="b-slideshow slider-wrapper theme-default">
        <div class="slideshow-content nivoSlider" id="slider">

    <?php foreach ($items as $key=>$item):
				$regex = "/<img[^>]+src\s*=\s*[\"']\/?([^\"']+)[\"'][^>]*\>/";

	?>
 
<? if ($item->extra_fields[0]) :
	preg_match ($regex, $item->extra_fields[0]->value, $matches); ?>
   <img style="display: none;" src="<?php echo ($matches[1]); ?>" alt="image" title='<?php echo $item->extra_fields[1]->value; ?>'>			
<?php endif; ?>

<? if ($item->extra_fields[2]) :
	preg_match ($regex, $item->extra_fields[2]->value, $matches); ?>
   <img style="display: none;" src="<?php echo ($matches[1]); ?>" alt="image" title='<?php echo $item->extra_fields[3]->value; ?>'>			
<?php endif; ?>
		
	
<? if ($item->extra_fields[4]) :
	preg_match ($regex, $item->extra_fields[4]->value, $matches); ?>
   <img style="display: none;" src="<?php echo ($matches[1]); ?>" alt="image" title='<?php echo $item->extra_fields[5]->value; ?>'>			
<?php endif; ?>	
	
<? if ($item->extra_fields[6]) :
	preg_match ($regex, $item->extra_fields[6]->value, $matches); ?>
   <img style="display: none;" src="<?php echo ($matches[1]); ?>" alt="image" title='<?php echo $item->extra_fields[7]->value; ?>'>			
<?php endif; ?>		
     
<? if ($item->extra_fields[8]) :
	preg_match ($regex, $item->extra_fields[8]->value, $matches); ?>
   <img style="display: none;" src="<?php echo ($matches[1]); ?>" alt="image" title='<?php echo $item->extra_fields[9]->value; ?>'>			
<?php endif; ?>		 
	 
<? if ($item->extra_fields[10]) :
	preg_match ($regex, $item->extra_fields[10]->value, $matches); ?>
   <img style="display: none;" src="<?php echo ($matches[1]); ?>" alt="image" title='<?php echo $item->extra_fields[11]->value; ?>'			
<?php endif; ?>		

<? if ($item->extra_fields[12]) : 
preg_match ($regex, $item->extra_fields[12]->value, $matches); ?>
   <img style="display: none;" src="<?php echo ($matches[1]); ?>" alt="image" title='<?php echo $item->extra_fields[13]->value; ?>'			
<?php endif; ?>	

<? if ($item->extra_fields[14]) : 
	preg_match ($regex, $item->extra_fields[14]->value, $matches); ?>
   <img style="display: none;" src="<?php echo ($matches[1]); ?>" alt="image" title='<?php echo $item->extra_fields[15]->value; ?>'			
<?php endif; ?>	

<? if ($item->extra_fields[16]) : 
	preg_match ($regex, $item->extra_fields[16]->value, $matches); ?>
   <img style="display: none;" src="<?php echo ($matches[1]); ?>" alt="image" title='<?php echo $item->extra_fields[17]->value; ?>'			
<?php endif; ?>	
	 
<? if ($item->extra_fields[18]) : 
	preg_match ($regex, $item->extra_fields[18]->value, $matches); ?>
   <img style="display: none;" src="<?php echo ($matches[1]); ?>" alt="image" title='<?php echo $item->extra_fields[19]->value; ?>'			
<?php endif; ?>		 
	 
<? if ($item->extra_fields[20]) : 
	preg_match ($regex, $item->extra_fields[20]->value, $matches); ?>
   <img style="display: none;" src="<?php echo ($matches[1]); ?>" alt="image" title='<?php echo $item->extra_fields[21]->value; ?>'			
<?php endif; ?>		 
	 
	 
	 
	 

    <?php endforeach; ?>
 
  
  </div>
        <div class="slideshow-borders"></div>
	</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#slider').nivoSlider({
            effect: 'fade', // Specify sets like: 'fold,fade,sliceDown'
            slices: 15, // For slice animations
            boxCols: 8, // For box animations
            boxRows: 4, // For box animations
            animSpeed: 500, // Slide transition speed
            pauseTime: 3000, // How long each slide will show
            startSlide: 0, // Set starting Slide (0 index)
            directionNav: true, // Next & Prev navigation
            controlNav: true, // 1,2,3... navigation
            controlNavThumbs: false, // Use thumbnails for Control Nav
            pauseOnHover: true, // Stop animation while hovering
            manualAdvance: false, // Force manual transitions
            prevText: 'Prev', // Prev directionNav text
            nextText: 'Next', // Next directionNav text
            randomStart: false, // Start on a random slide
            beforeChange: function(){}, // Triggers before a slide transition
            afterChange: function(){}, // Triggers after a slide transition
            slideshowEnd: function(){}, // Triggers after all slides have been shown
            lastSlide: function(){}, // Triggers when last slide is shown
            afterLoad: function(){} // Triggers when slider has loaded
        });

            });
</script>

	
