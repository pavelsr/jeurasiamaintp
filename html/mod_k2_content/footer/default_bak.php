<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
$lang = JRequest::getVar('lang', null); 
//echo $lang;
if ($lang == 'ru-RU') { 
$zag = 'Погода в Санкт-Петербурге';
$dayz = 'день';
$nightz = 'ночь';
} else {
$zag = 'Weather in St-Petersburg';
$dayz = 'day';
$nightz = 'night';
}

?>


<?
function getWeather()
{
    $weather = getGismeteoWeather(26063);

    $weatherDayClass = getWeatherClass($weather['CLOUDINESS'], $weather['PRECIPITATION']);
    $weatherNightClass = getWeatherClass($weather['CLOUDINESS_NIGHT'], $weather['PRECIPITATION_NIGHT']);

    return Array(
        "TEMP" => $weather["TEMP"],
        "CLASS" => $weatherDayClass['CLASS'],
        "NAME" => $weatherDayClass['NAME'],
        "TEMP_NIGHT" => $weather["TEMP_NIGHT"],
        "CLASS_NIGHT" => $weatherNightClass['CLASS'],
        "NAME_NIGHT" => $weatherNightClass['NAME']
    );
}

function getWeatherClass($cloudiness, $precipitation)
{
    $weather['CLASS'] = "";
    $weather['NAME'] = "";
    switch ($cloudiness) {
        case 0:
            $weather['CLASS'] == 'sunny';
            $weather['NAME'] = 'безоблачно';
            break;
        case 1:
            $weather['CLASS'] = 'small-clouds';
            $weather['NAME'] = 'облачность';
            break;
        case 2:
            $weather['CLASS'] = 'cloudy';
            $weather['NAME'] = 'облачно';
            break;
        case 3:
            $weather['CLASS'] = 'big-cloud';
            $weather['NAME'] = 'облачно';
            break;
    }
    switch ($precipitation) {
        case 4:
            $weather['CLASS'] = "rain";
            $weather['NAME'] = 'дожди';
            break;
        case 5:
            $weather['CLASS'] = "rainfall";
            $weather['NAME'] = 'ливень';
            break;
        case 6:
            $weather['CLASS'] = "snow";
            $weather['NAME'] = 'снег';
            break;
        case 7:
            $weather['CLASS'] = "snow";
            $weather['NAME'] = 'снег';
            break;
        case 8:
            $weather['CLASS'] = "storm";
            $weather['NAME'] = 'щторм';
            break;
        case 9:
            $weather['CLASS'] = "unknown";
            $weather['NAME'] = '';
            break;
        case 10:
            $weather['CLASS'] = "clear";
            $weather['NAME'] = 'ясно';
            break;
    }

    return $weather;
}



function getMinTemperature($weather)
{
    $tempMax = (int)$weather->TEMPERATURE->attributes()->max;
    $tempMin = (int)$weather->TEMPERATURE->attributes()->min;
    $temp = (int)round(($tempMax + $tempMin) / 2, 0);
    return ($temp > 0) ? '+' . (string)$temp : (string)$temp;
}

function getCloudiness($weather)
{
    return (int)$weather->PHENOMENA->attributes()->cloudiness;
}

function getPrecipitation($weather)
{
    return (int)$weather->PHENOMENA->attributes()->precipitation;
}

function getGismeteoWeather($cityId)
{
    $temp = "?";
    $cloudiness = 0;
    $precipitation = 10;
    $city_id = 26063; // id города
    $data_file = "http://export.yandex.ru/weather-ng/forecasts/$city_id.xml"; // адрес xml файла
    $xml = simplexml_load_file($data_file); // раскладываем xml на массив
    $tempDay = $xml->day[0]->day_part[1]->{'temperature-data'}->avg;
    ($tempDay > 0) ? $tempDay = '+' . (string)$tempDay : (string)$tempDay;
    $tempNight = $xml->day[0]->day_part[3]->{'temperature-data'}->avg;
    ($tempNight > 0) ? $tempNight = '+' . (string)$tempNight : (string)$tempNight;

    return Array(
        "TEMP" => $tempDay,
        "CLOUDINESS" => $cloudiness,
        "PRECIPITATION" => $precipitation,
        "TEMP_NIGHT" => $tempNight,
        "CLOUDINESS_NIGHT" => $cloudiness_night,
        "PRECIPITATION_NIGHT" => $precipitation_night
    );
}




try {
$weather = getWeather();
}
 catch (Exception $e) {
    echo 'parse error';
}

$cache = & JFactory::getCache('ya_weather');
$cache->setCaching(1);  
$cache->setLifeTime(60);
$weather = $cache->call( 'getWeather') ;



?>






	 <?php foreach ($items as $key=>$item):
foreach ($item->extra_fields as $key=>$fields):	
if ($fields->id == '33') {
$copyright = $fields->value;
}
endforeach;
	?>
			<div class="footer_top">
				<div class="foot_contacts">
			<?php echo $item->introtext; ?>
				</div>
				<div class="foot_banners">
			<?php echo $item->fulltext; ?>
				</div>
<div class="b-utils">
<?
	    if ($lang != 'ru-RU') { 
        $weather['NAME'] = $weather['CLASS'];
    }
?>

    <div class="weather clearfix">
        <div class="weather__city">
             <?php echo $zag; ?>
        </div>
        <div class="weather__data">
            <div class="weather__current">
                <span class="weather-hour"><?php echo $dayz; ?></span>
                <span class="weather-temp"><? echo $weather["TEMP"] ?></span><br/>
                <span class="weather-name"><? echo $weather["NAME"] ?></span>
            </div>
            <div class="weather__icon">
                <span class="weather-icon <? echo $weather["CLASS"] ?>"></span>
            </div>
            <div class="weather__night">
                <span class="weather-hour"><?php echo $nightz; ?></span>
                <span class="weather-temp night"><? echo $weather["TEMP_NIGHT"] ?></span><br/>
                <span style="line-height: 16px;" class="weather-name">
				<? 
				if ($lang != 'ru-RU') {
				if ($weather["NAME_NIGHT"] = 'безоблачно') {
				$weather["NAME_NIGHT"] = 'sunny';
				}
				if ($weather["NAME_NIGHT"] = 'облачность') {
				$weather["NAME_NIGHT"] = 'small-clouds';
				}
				if ($weather["NAME_NIGHT"] = 'облачно') {
				$weather["NAME_NIGHT"] = 'cloudy';
				}
				if ($weather["NAME_NIGHT"] = 'дожди') {
				$weather["NAME_NIGHT"] = 'rain';
				}
				if ($weather["NAME_NIGHT"] = 'ливень') {
				$weather["NAME_NIGHT"] = 'rainfall';
				}
				if ($weather["NAME_NIGHT"] = 'снег') {
				$weather["NAME_NIGHT"] = 'snow';
				}
				if ($weather["NAME_NIGHT"] = 'шторм') {
				$weather["NAME_NIGHT"] = 'storm';
				}
				if ($weather["NAME_NIGHT"] = '') {
				$weather["NAME_NIGHT"] = 'unknown';
				}
				if ($weather["NAME_NIGHT"] = 'ясно') {
				$weather["NAME_NIGHT"] = 'clear';
				}
				}
				echo $weather["NAME_NIGHT"]; ?></span>
            </div>
            <div style="clear: both;"></div>
        </div>
    </div>

  <?php
  $document   = & JFactory::getDocument();
$renderer   = $document->loadRenderer('modules');
$options    = array('style' => 'title');
$position   = 'curr';
echo $renderer->render($position, $options, null);
?>                 



</div>				
				
			
			</div>
			<div class="footer-bottom">
				<div class="copyright"><?php echo $copyright; ?></div>
			</div>
			 
	 
	 <?php endforeach; ?>					
     
