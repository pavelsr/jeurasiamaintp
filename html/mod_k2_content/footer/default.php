<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
$lang = JRequest::getVar('lang', null); 
//echo $lang;
if ($lang == 'ru-RU') { 
$zag = 'Погода в Санкт-Петербурге';
$dayz = 'день';
$nightz = 'ночь';
} else {
$zag = 'Weather in St-Petersburg';
$dayz = 'day';
$nightz = 'night';
}

?>


<?
function getWeather()
{	
	$cache = & JFactory::getCache();
	$cache->setCaching( 1 );
	$cache->setLifeTime(10);
	$weather = $cache->call( 'getGismeteoWeather', 26063 );

    $weatherDayClass = getWeatherClass($weather['CLOUDINESS'], $weather['PRECIPITATION']);
    $weatherNightClass = getWeatherClass($weather['CLOUDINESS_NIGHT'], $weather['PRECIPITATION_NIGHT']);

    return Array(
        "TEMP" => $weather["TEMP"],
        "CLASS" => $weatherDayClass['CLASS'],
        "NAME" => $weatherDayClass['NAME'],
        "TEMP_NIGHT" => $weather["TEMP_NIGHT"],
        "CLASS_NIGHT" => $weatherNightClass['CLASS'],
        "NAME_NIGHT" => $weatherNightClass['NAME']
    );
}

function getWeatherClass($cloudiness, $precipitation)
{
    $weather['CLASS'] = "";
    $weather['NAME'] = "";
    switch ($cloudiness) {
        case 0:
            $weather['CLASS'] == 'sunny';
            $weather['NAME'] = 'безоблачно';
            break;
        case 1:
            $weather['CLASS'] = 'small-clouds';
            $weather['NAME'] = 'облачность';
            break;
        case 2:
            $weather['CLASS'] = 'cloudy';
            $weather['NAME'] = 'облачно';
            break;
        case 3:
            $weather['CLASS'] = 'big-cloud';
            $weather['NAME'] = 'облачно';
            break;
    }
    switch ($precipitation) {
        case 4:
            $weather['CLASS'] = "rain";
            $weather['NAME'] = 'дожди';
            break;
        case 5:
            $weather['CLASS'] = "rainfall";
            $weather['NAME'] = 'ливень';
            break;
        case 6:
            $weather['CLASS'] = "snow";
            $weather['NAME'] = 'снег';
            break;
        case 7:
            $weather['CLASS'] = "snow";
            $weather['NAME'] = 'снег';
            break;
        case 8:
            $weather['CLASS'] = "storm";
            $weather['NAME'] = 'щторм';
            break;
        case 9:
            $weather['CLASS'] = "unknown";
            $weather['NAME'] = '';
            break;
        case 10:
            $weather['CLASS'] = "clear";
            $weather['NAME'] = 'ясно';
            break;
    }

    return $weather;
}



function getMinTemperature($weather)
{
    $tempMax = (int)$weather->TEMPERATURE->attributes()->max;
    $tempMin = (int)$weather->TEMPERATURE->attributes()->min;
    $temp = (int)round(($tempMax + $tempMin) / 2, 0);
    return ($temp > 0) ? '+' . (string)$temp : (string)$temp;
}

function getCloudiness($weather)
{
    return (int)$weather->PHENOMENA->attributes()->cloudiness;
}

function getPrecipitation($weather)
{
    return (int)$weather->PHENOMENA->attributes()->precipitation;
}

function getGismeteoWeather_old($cityId)
{
    $temp = "?";
    $cloudiness = 0;
    $precipitation = 10;
    $city_id = 26063; // id города
    $data_file = "http://export.yandex.ru/weather-ng/forecasts/$city_id.xml"; 
    $xml = simplexml_load_file($data_file); // раскладываем xml на массив
    $tempDay = $xml->day[0]->day_part[1]->{'temperature-data'}->avg;
    ($tempDay > 0) ? $tempDay = '+' . (string)$tempDay : (string)$tempDay;
    $tempNight = $xml->day[0]->day_part[3]->{'temperature-data'}->avg;
    ($tempNight > 0) ? $tempNight = '+' . (string)$tempNight : (string)$tempNight;

    return Array(
        "TEMP" => $tempDay,
        "CLOUDINESS" => $cloudiness,
        "PRECIPITATION" => $precipitation,
        "TEMP_NIGHT" => $tempNight,
        "CLOUDINESS_NIGHT" => $cloudiness_night,
        "PRECIPITATION_NIGHT" => $precipitation_night
    );
}

function getGismeteoWeather($cityId)
{
    $temp = "?";
    $cloudiness = 0;
    $precipitation = 10;
    $weather = simplexml_load_file("http://informer.gismeteo.ru/xml/" . $cityId . ".xml");
    if ($weather) {
        for ($i = 0; $i < 4; $i++) {
            if ($weather->REPORT->TOWN->FORECAST[$i]) {
            if ((int)$weather->REPORT->TOWN->FORECAST[$i]->attributes()->hour == 15) {
                $temp = getMinTemperature($weather->REPORT->TOWN->FORECAST[$i]);
                $cloudiness = getCloudiness($weather->REPORT->TOWN->FORECAST[$i]);
                $precipitation = getPrecipitation($weather->REPORT->TOWN->FORECAST[$i]);
            } else if ((int)$weather->REPORT->TOWN->FORECAST[$i]->attributes()->hour == 3) {
                $temp_night = getMinTemperature($weather->REPORT->TOWN->FORECAST[$i]);
                $cloudiness_night = getCloudiness($weather->REPORT->TOWN->FORECAST[$i]);
                $precipitation_night = getPrecipitation($weather->REPORT->TOWN->FORECAST[$i]);
            }
            }
        }
    }

    return Array(
        "TEMP" => $temp,
        "CLOUDINESS" => $cloudiness,
        "PRECIPITATION" => $precipitation,
        "TEMP_NIGHT" => $temp_night,
        "CLOUDINESS_NIGHT" => $cloudiness_night,
        "PRECIPITATION_NIGHT" => $precipitation_night
    );
}




try {
$weather = getWeather();
}
 catch (Exception $e) {
    echo 'parse error';
}

?>






	 <?php foreach ($items as $key=>$item):
foreach ($item->extra_fields as $key=>$fields):	
if ($fields->id == '33') {
$copyright = $fields->value;
}
if ($fields->id == '34') {
$copyright_before = $fields->value;
}
endforeach;
	?>
			<div class="footer_top">
				<div class="foot_contacts">
			<?php echo $item->introtext; ?>
				</div>
				<div class="foot_banners">
			<?php echo $item->fulltext; ?>
				</div>
				<div class="foot_social">
					<div class="soc_header">Мы в социальных сетях</div>
					<div class="soc_link">
	          <a onclick="pklCounterInvolv('social')" href="http://vk.com/club350602" rel="nofollow" title="Vkontakte" class="vkont" target="_blank"></a>
	          <a onclick="pklCounterInvolv('social')" href="https://www.ok.ru/group/56394252026107" rel="nofollow" title="Odnoklassniki" class="odnoklassniki" target="_blank"></a>
	          <a onclick="pklCounterInvolv('social')" href="https://www.youtube.com/channel/UCjb68jqMRDdq9-YTyDMoT8A/featured" rel="nofollow" title="youtube" class="youtube" target="_blank"></a>
	          <a onclick="pklCounterInvolv('social')" href="https://www.tiktok.com/@eurasia_hotels_group" rel="nofollow" title="TikTok" class="tiktok" target="_blank"></a>
	          <a onclick="pklCounterAttempt('messenger')" href="https://wa.me/78126445347" rel="nofollow" title="Whatsapp" class="wapp" target="_blank"></a>
	        </div>
				</div>
				
<div class="b-utils">
<?
	    if ($lang != 'ru-RU') { 
        $weather['NAME'] = $weather['CLASS'];
    }
?>


  <?php
  $document   = & JFactory::getDocument();
$renderer   = $document->loadRenderer('modules');
$options    = array('style' => 'title');
$position   = 'curr';
echo $renderer->render($position, $options, null);
?>                 



</div>				
				
			
			</div>
			<div class="footer-bottom">
				<?php echo $copyright_before; ?>
				<div class="copyright"><?php echo $copyright; ?></div>
			</div>
			 
	 
	 <?php endforeach; ?>					
     
