<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>
<?php
$regex = "/<img[^>]+src\s*=\s*[\"']\/?([^\"']+)[\"'][^>]*\>/"; 
$regex1 ='/href="([^"]+)"/';
 foreach ($items as $key=>$item):
	foreach ($item->extra_fields as $key=>$fields):
	if ($fields->id == '45') {
		preg_match ($regex, $fields->value, $image1);
	}
	if ($fields->id == '46') {
		$link1 = $fields->value;
		preg_match ($regex1, $fields->value, $href1);
	}
	
	if ($fields->id == '47') {
		preg_match ($regex, $fields->value, $image2);
	}
	if ($fields->id == '48') {
		$link2 = $fields->value;
		preg_match ($regex1, $fields->value, $href2);
	}	
	
	if ($fields->id == '49') {
		preg_match ($regex, $fields->value, $image3);
	}
	if ($fields->id == '50') {
		$link3 = $fields->value;
		preg_match ($regex1, $fields->value, $href3);
	}	
	
	if ($fields->id == '51') {
		preg_match ($regex, $fields->value, $image4);
	}
	if ($fields->id == '52') {
		$link4 = $fields->value;
		preg_match ($regex1, $fields->value, $href4);
	}	
	
	if ($fields->id == '53') {
		preg_match ($regex, $fields->value, $image5);
	}
	if ($fields->id == '54') {
		$link5 = $fields->value;
		preg_match ($regex1, $fields->value, $href5);
	}	
	
	if ($fields->id == '55') {
		preg_match ($regex, $fields->value, $image6);
	}
	if ($fields->id == '56') {
		$link6 = $fields->value;
		preg_match ($regex1, $fields->value, $href6);
	}	
	
	if ($fields->id == '57') {
		preg_match ($regex, $fields->value, $image7);
	}
	if ($fields->id == '58') {
		$link7 = $fields->value;
		preg_match ($regex1, $fields->value, $href7);
	}	
	
	if ($fields->id == '59') {
		preg_match ($regex, $fields->value, $image8);
	}
	if ($fields->id == '60') {
		$link8 = $fields->value;
		preg_match ($regex1, $fields->value, $href8);
	}
	
	if ($fields->id == '61') {
		preg_match ($regex, $fields->value, $image9);
	}
	if ($fields->id == '62') {
		$link9 = $fields->value;
		preg_match ($regex1, $fields->value, $href9);
	}	
	
	if ($fields->id == '63') {
		preg_match ($regex, $fields->value, $image10);
	}
	if ($fields->id == '64') {
		$link10 = $fields->value;
		preg_match ($regex1, $fields->value, $href10);
	}	
 
 endforeach;
endforeach;
 ?>		


 <link rel="stylesheet" type="text/css" href="/templates/main/css/main_slider.css">

    <div class="b-slideshow slider-wrapper theme-default">
        <div class="slideshow-content nivoSlider" id="slider">
		<?php if ($image1[1] != '') { ?>
            <a href="<?php echo $href1[1]; ?>">
                <img src="<? echo $image1[1]; ?>" style="display: none;" alt="" title="#htmlcaption1"/>
            </a>
		<?php } ?>
		
		<?php if ($image2[1] != '') { ?>
            <a href="<?php echo $href2[1]; ?>">
                <img src="<? echo $image2[1]; ?>" style="display: none;" alt="" title="#htmlcaption2"/>
            </a>
		<?php } ?>
		
		<?php if ($image3[1] != '') { ?>
            <a href="<?php echo $href3[1]; ?>">
                <img src="<? echo $image3[1]; ?>" style="display: none;" alt="" title="#htmlcaption3"/>
            </a>
		<?php } ?>
		
		<?php if ($image4[1] != '') { ?>
            <a href="<?php echo $href4[1]; ?>">
                <img src="<? echo $image4[1]; ?>" style="display: none;" alt="" title="#htmlcaption4"/>
            </a>
		<?php } ?>
		
		<?php if ($image5[1] != '') { ?>
            <a href="<?php echo $href5[1]; ?>">
                <img src="<? echo $image5[1]; ?>" style="display: none;" alt="" title="#htmlcaption5"/>
            </a>
		<?php } ?>
		
		<?php if ($image6[1] != '') { ?>
            <a href="<?php echo $href6[1]; ?>">
                <img src="<? echo $image6[1]; ?>" style="display: none;" alt="" title="#htmlcaption6"/>
            </a>
		<?php } ?>
		
		<?php if ($image7[1] != '') { ?>
            <a href="<?php echo $href7[1]; ?>">
                <img src="<? echo $image7[1]; ?>" style="display: none;" alt="" title="#htmlcaption7"/>
            </a>
		<?php } ?>
		
		<?php if ($image8[1] != '') { ?>
            <a href="<?php echo $href8[1]; ?>">
                <img src="<? echo $image8[1]; ?>" style="display: none;" alt="" title="#htmlcaption8"/>
            </a>
		<?php } ?>
		
		<?php if ($image9[1] != '') { ?>
            <a href="<?php echo $href9[1]; ?>">
                <img src="<? echo $image9[1]; ?>" style="display: none;" alt="" title="#htmlcaption9"/>
            </a>
		<?php } ?>
		
		<?php if ($image10[1] != '') { ?>
            <a href="<?php echo $href10[1]; ?>">
                <img src="<? echo $image10[1]; ?>" style="display: none;" alt="" title="#htmlcaption10"/>
            </a>
		<?php } ?>
		
		
		
		
        </div>
        <div class="slideshow-borders"></div>
        <div class="slideshow-decor"></div>
		<?php if ($image1[1] != '') { ?>
        <div id="htmlcaption1" class="nivo-html-caption">
            <p><?php echo $link1; ?></p>
        </div>
		<?php } ?>
		
		<?php if ($image2[1] != '') { ?>
        <div id="htmlcaption2" class="nivo-html-caption">
            <p><?php echo $link2; ?></p>
        </div>
		<?php } ?>
		
		<?php if ($image3[1] != '') { ?>
        <div id="htmlcaption3" class="nivo-html-caption">
            <p><?php echo $link3; ?></p>
        </div>
		<?php } ?>
		
		<?php if ($image4[1] != '') { ?>
        <div id="htmlcaption4" class="nivo-html-caption">
            <p><?php echo $link4; ?></p>
        </div>
		<?php } ?>
		
		<?php if ($image5[1] != '') { ?>
        <div id="htmlcaption5" class="nivo-html-caption">
            <p><?php echo $link5; ?></p>
        </div>
		<?php } ?>
		
		<?php if ($image6[1] != '') { ?>
        <div id="htmlcaption6" class="nivo-html-caption">
            <p><?php echo $link6; ?></p>
        </div>
		<?php } ?>
		
		<?php if ($image7[1] != '') { ?>
        <div id="htmlcaption7" class="nivo-html-caption">
            <p><?php echo $link7; ?></p>
        </div>
		<?php } ?>		
		
		<?php if ($image8[1] != '') { ?>
        <div id="htmlcaption8" class="nivo-html-caption">
            <p><?php echo $link8; ?></p>
        </div>
		<?php } ?>		
		
		<?php if ($image9[1] != '') { ?>
        <div id="htmlcaption9" class="nivo-html-caption">
            <p><?php echo $link9; ?></p>
        </div>
		<?php } ?>		
		
		<?php if ($image10[1] != '') { ?>
        <div id="htmlcaption10" class="nivo-html-caption">
            <p><?php echo $link10; ?></p>
        </div>
		<?php } ?>		
		
		
    </div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#slider').nivoSlider({
            effect: 'fade', // Specify sets like: 'fold,fade,sliceDown'
            slices: 15, // For slice animations
            boxCols: 8, // For box animations
            boxRows: 4, // For box animations
            animSpeed: 500, // Slide transition speed
            pauseTime: 3000, // How long each slide will show
            startSlide: 0, // Set starting Slide (0 index)
            directionNav: true, // Next & Prev navigation
            controlNav: true, // 1,2,3... navigation
            controlNavThumbs: false, // Use thumbnails for Control Nav
            pauseOnHover: true, // Stop animation while hovering
            manualAdvance: false, // Force manual transitions
            prevText: 'Prev', // Prev directionNav text
            nextText: 'Next', // Next directionNav text
            randomStart: false, // Start on a random slide
            beforeChange: function(){}, // Triggers before a slide transition
            afterChange: function(){}, // Triggers after a slide transition
            slideshowEnd: function(){}, // Triggers after all slides have been shown
            lastSlide: function(){}, // Triggers when last slide is shown
            afterLoad: function(){} // Triggers when slider has loaded
        });

            });
</script>
