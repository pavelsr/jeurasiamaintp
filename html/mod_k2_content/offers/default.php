<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>
<?php $lang = JRequest::getVar('lang', null);  ?>

	<?php if(count($items)): ?>
  <ul class="list clearfix">
    <?php foreach ($items as $key=>$item):	
	$titlez = '';
	foreach ($item->extra_fields as $key=>$fields):
	if ($fields->id == '40') {
		$titlez = $fields->value;
	}	
if ($fields->id == '39') {	
	$link_one = $fields->value;
}
if ($fields->id == '66') {	
	$section = $fields->value;
} else {
	$section = '';
}

	endforeach;
	if ($titlez != '') {
		$zag = $titlez;
	} else {
		$zag = $item->title;
	}	
	
$linkz1 = substr_replace($item->link, '', 0, 1);

if ($lang == 'en-GB') {
$linkz = '/en';
} else if ($lang == 'fr-FR') {
$linkz = '/fr';	
} else {
$linkz = '';
}

?>
	<?php
	$pos = strripos($section, 'главной');
	if ($pos) {
	$z++;	
	?>
	
    <li class="item">
<div class="w-shadow">
<?php if ($item->featured == '1') { ?>
<a title="<?php echo strip_tags($zag); ?>" href="<?php echo $link_one; ?>">
<?php } else { ?>
<a title="<?php echo strip_tags($zag); ?>" href="<?php echo $linkz; ?>/special-offers/?ID=<?php echo $item->id;?>">
<?php } ?>
       <i class="b-shadow"></i>
		<img alt="" src="<?php echo $item->image; ?>" class="b-img">
        <span class="w-anonce">
          <span class="anonce"><?php echo $zag; ?> </span></span>
                </a>	
     </div>
    </li>
	<?php 
/*	if ($z == '4') {
		break; 
	} * pkl slider */	
	} ?>  
    <?php endforeach;	?>
    </ul>
  <?php endif; ?>
