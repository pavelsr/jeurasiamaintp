<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>

                   <ul class="news-list">
	 <?php foreach ($items as $key=>$item):	?>
                        <li class="news-item">
<div class="news-date"><?php echo JHTML::_('date', $item->created, 'd.m.y'); ?></div>
		
			        <div class="news-name">
                      <a title="<?php echo $item->title; ?>" href="<?php echo $item->link; ?>" class="news-name-link"><?php echo $item->title; ?></a>
                  </div>
                        </li>
	 <?php endforeach; ?>					
                    </ul>
