<?php
 /**
 * @package mod_jlcurrency
 * @author Zhukov Artem (artem@joomline.ru)
 * @version 1.1
 * @copyright (C) 2012 by JoomLine (http://www.joomline.net)
 * @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html
 *
*/
$lang = JRequest::getVar('lang', null); 
if ($lang == 'ru-RU') { 
$zag = 'Курс валют';
$rub = 'руб.';
} else {
$zag = 'Currency';
$rub = 'rub.';
}
?>
    <div class="b-currancy-rate">
        <div style='text-align: right;color: #000;'><?php echo $zag; ?></div>
        <div class="b-currancy-rate__data">
	<? foreach ($data as $curr) {  ?>
	<?php if ($curr['CharCode'] == 'USD') { 
	$doll = str_replace(",", ".", $curr['Value']);
	$doll = round($doll, 2);
	 }
	 if ($curr['CharCode'] == 'EUR') {
	$euro = str_replace(",", ".", $curr['Value']);
	$euro = round($euro, 2);
	 }
	 
	}	 ?>
            <span class="b-currancy-symbol" style="padding-left:0;">$</span><span
                class="b-currancy-value"><?echo $doll;?> <?php echo $rub; ?></span>
	
            <span class="b-currancy-symbol">€</span><span
                class="b-currancy-value"><?echo $euro;?> <?php echo $rub; ?></span>

        </div>
    </div>
	
