<?php
defined('_JEXEC') or die;
//JHtml::_('bootstrap.framework');
//JHtml::_('bootstrap.loadCss', false, $this->direction);
?>
<?php
$Itemid = JRequest::getVar('Itemid', null);
$option = JRequest::getVar('option', null);
$view = JRequest::getVar('view', null);
$page = JRequest::getVar('page', null);
$lang = JRequest::getVar('lang', null);
$chronoform = JRequest::getVar('chronoform', null);
$document 	= & JFactory::getDocument();
$user = & JFactory::getUser();
$app				= JFactory::getApplication();
$templateparams		= $app->getTemplate(true)->params;
$menu = JFactory::getApplication()->getMenu();
$active = $menu->getActive();
$menuname = $active->title;
$parentId = $active->tree[0];
$parentId1 = $active->tree[1];
$pageClassSuffix = JSite::getMenu()->getActive()? JSite::getMenu()->getActive()->params->get('pageclass_sfx', '-default') : '-default';
$parent1   = $menu->getItem($parentId1);

if ($parent1 !='') {
$hotel_name = $parent1->params->get('menu-anchor_css');
}
 if (($option == 'com_content') or ($option == 'com_k2')) {
 $page_suf = $active->params->get('pageclass_sfx');
 }


	if ($lang == 'en-GB') {
	$lang_z = 'en-US';
	$close = 'Close';
	$map = 'Map';
	$search_room = 'Search rooms';
	$hidden_form = 'Hide';
	$expand = 'Expand';
	} else if ($lang == 'fr-FR') {
	//$lang_z = 'fr-FR'; Yandex не поддерживает pkl
	$lang_z = 'en-US';
	$close = 'Close';
	$map = 'Map';
	$search_room = 'Trouver les chambres';
	$hidden_form = 'Cacher';
	$expand = 'Étendre';
	} else if ($lang == 'de-DE') {
	$lang_z = 'de-DE';
	$close = 'Close';
	$map = 'Map';
	$search_room = 'Finden Sie die Zahlen';
	$hidden_form = 'Ausblenden';
	$expand = 'Zu erweitern';
	} else {
	$lang_z = 'ru-RU';
	$close = 'Закрыть';
	$map = 'Карта';
	$search_room = 'Найти номера';
	$hidden_form = 'Скрыть';
	$expand = 'Развернуть';
	}
	?>
<!DOCTYPE HTML>
<html>
<head>
<?php
if ($user->id == '') {
	unset($this->_scripts[$this->baseurl.'/media/system/js/mootools-core.js'],
	$this->_scripts[$this->baseurl.'/media/system/js/mootools-more.js'],
	$this->_scripts[$this->baseurl.'/media/system/js/core.js'],
	$this->_scripts[$this->baseurl.'/media/jui/js/bootstrap.min.js'],
	$this->_scripts[$this->baseurl.'/media/jui/js/jquery.min.js'],
	$this->_scripts[$this->baseurl.'/media/system/js/caption.js'],
	$this->_scripts[$this->baseurl.'/media/system/js/modal.js'],
	$this->_scripts[$this->baseurl.'/media/jui/js/jquery-noconflict.js'],
	$this->_scripts[$this->baseurl.'/media/jui/js/jquery-migrate.min.js'],
	$this->_scripts[$this->baseurl.'/components/com_k2/js/k2.js?v2.6.9&amp;sitepath=/']);
} else { ?>
	    <script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<? } ?>
<jdoc:include type="head" />
<meta name="viewport" content="width=device-width">

<link rel="shortcut icon" href="/images/favicon.ico">
<link rel="stylesheet" type="text/css" href="templates/main/css/template.css?ver=126">
<link rel="stylesheet" type="text/css" href="templates/main/css/style.css">
<?php if (($parentId == '113') or ($parentId == '253') or ($parentId == '337') or ($parentId == '411')) { ?>
<link rel="stylesheet" type="text/css" href="templates/main/css/template2.css">
<?php } ?>
<link rel="stylesheet" href="/js/nivo/nivo-slider.css" media="screen">
<link rel="stylesheet" type="text/css" href="templates/main/css/mobile.css">

<?php if ($lang == 'ru-RU') { ?>
<link rel="stylesheet" type="text/css" href="templates/main/css/style_ru.css">
<?php } else { ?>
<link rel="stylesheet" type="text/css" href="templates/main/css/style_en.css">
<?php } ?>

        <script type="text/javascript">
		var DEFER_VARS = DEFER_VARS || {};
		var DEFER_GAL1 = false;
		<?php if ($lang == 'en-GB') { ?>
		DEFER_VARS.INCLUDE_OFFICIAL_GROUPS = {'close': "Close"};
		<?php } else if ($lang == 'fr-FR') { ?>
		DEFER_VARS.INCLUDE_OFFICIAL_GROUPS = {'close': "Près"};
		<?php } else if ($lang == 'de-DE') { ?>
		DEFER_VARS.INCLUDE_OFFICIAL_GROUPS = {'close': "Schliessen"};
		<?php } else { ?>
		DEFER_VARS.INCLUDE_OFFICIAL_GROUPS = {'close': "Закрыть"};
		<?php } ?>
		</script>
<?php if ((($parentId == '171') or ($parentId == '251') or ($parentId == '335') or ($parentId == '409')) and empty($parentId1)) { ?>
    <script data-cfasync="false" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<?php } else { ?>
    <script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
<?php } ?>

    <script type="text/javascript" src="/js/jquery.carouFredSel-6.2.1-packed.js" defer></script>
    <script type="text/javascript" src="/js/nivo/jquery.nivo.slider.pack.js" defer></script>
    <script type="text/javascript" src="/js/colorbox/jquery.colorbox-min.js" defer></script>
    <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=<?php echo$lang_z; ?>" type="text/javascript" defer></script>
    <script type="text/javascript" src="/js/jquery-ui-1.10.4.custom.js" defer></script>
   <script type="text/javascript" src="/js/main.min.js" defer></script>
   <script type="text/javascript" src="js/jquery.modal.min.js" defer></script>
   <script type="text/javascript" src="js/jquery.cookie.min.js" defer></script>
   <script type="text/javascript" src="/templates/main/js/analytics-custom-dimensions.js" defer></script>

   <?php if ($user->id != '') { ?>

   <?php } ?>

    <!-- start TL head script -->
    <script type="text/javascript">
        (function(w){
            var q=[
                ['setContext', 'TL-INT-eurasia', '<?php echo $lang_tl; ?>']
            ];
            var t=w.travelline=(w.travelline||{}),ti=t.integration=(t.integration||{});ti.__cq=ti.__cq?ti.__cq.concat(q):q;
            if (!ti.__loader){ti.__loader=true;var d=w.document,p=d.location.protocol,s=d.createElement('script');s.type='text/javascript';s.async=true;s.src=(p=='https:'?p:'http:')+'//ibe.tlintegration.com/integration/loader.js';(d.getElementsByTagName('head')[0]||d.getElementsByTagName('body')[0]).appendChild(s);}
        })(window);
    </script>
    <!-- end TL head script -->

<?php if ($lang == 'ru-RU') { ?>
<!-- start CoMagic head script -->
<script type="text/javascript">
var __cs = __cs || [];
__cs.push(["setCsAccount", "pR6mB0g_ShHG7iGzqF9R1ulYldOeaTmt"]);
</script>
<script type="text/javascript" async src="https://app.comagic.ru/static/cs.min.js"></script>
<!-- end CoMagic head script -->
<?php } ?>

</head>

<?php
/*
echo '<h1>';
echo $Itemid;
echo $option;
echo $view;
echo $page;
echo $user->id;
echo $pageClassSuffix;
echo '</h1>';
*/
?>

<?php if ($hotel_name != '') { ?>
<body class="hot_<?php echo $hotel_name ?>">
<?php } else if ($page_suf != '') { ?>
<body class="<? echo $page_suf; ?>">
<?php } else { ?>
<body>
<?php } ?>
<jdoc:include type="modules" name="after_body"/>
<?php if (($parentId == '113') or ($parentId == '253') or ($parentId == '337') or ($parentId == '411')) { ?>
<div class="main b-page b-page_type_hotel">
<?php } else if (($pageClassSuffix == 'mainpage') and ($view == 'item')) { ?>
<div class="main b-page">
<?php } else { ?>
<div class="main b-page b-page_type_second">
<?php } ?>

		<link rel="stylesheet" type="text/css" href="templates/main/css/component.css" />
		<script src="templates/main/js/modernizr.custom.js"></script>
<div class="demo-1">
					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
<jdoc:include type="modules" name="mobilemenu"/>
					</div><!-- /dl-menuwrapper -->
		</div>
		<script src="templates/main/js/jquery.dlmenu.js"></script>

		<script>
	$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
				});
				});


			</script>



	<div class="head">
		<div class="wrap">
			<?php if ($lang == 'en-GB') { ?>
			<a href="/en/"><img class="logo_h" alt="" src="/images/hotel-group-logo.png"></a>
			<a href="/en/"><img class="logo_mobile" alt="" src="/images/hotel-group-logo.png"></a>
			<?php } else if ($lang == 'fr-FR') { ?>
			<a href="/fr/"><img alt="" src="/images/hotel-group-logo.png" class="logo_h"></a>
			<a href="/fr/"><img class="logo_mobile" alt="" src="/images/hotel-group-logo.png"></a>
			<?php } else if ($lang == 'de-DE') { ?>
			<a href="/de/"><img alt="" src="images/hotel-group-logo.png" class="logo_h"></a>
			<a href="/de/"><img class="logo_mobile" alt="" src="/images/hotel-group-logo.png"></a>
			<?php } else { ?>
			<a href="/"><img class="logo_h" alt="" src="/images/logo.png"></a>
			<a href="/"><img class="logo_mobile" alt="" src="/images/logo_mobile.png"></a>
			<?php } ?>
			<?php if ($hotel_name == 'dynasty') { ?>
			<a href="/">
         <?php if ($lang == 'ru-RU') { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/images/mobile_logo/dynasty_ru.png">
		 <?php } else { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/upload/iblock/e28/e28068b88f1a78bd89b4ebd1e7ca31d7.png">
			<?php } ?>
			</a>
			<?php } ?>

			<?php if ($hotel_name == 'amsterdam') { ?>
			<a href="/">
         <?php if ($lang == 'ru-RU') { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/upload/iblock/a54/a5458b79c46f6b6ef7cc4be0a8c3c7d5.png">
		 <?php } else { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/upload/iblock/1b6/1b61f7c2c88d85de8de3107c1956a201.png">
			<?php } ?>
			</a>
			<?php } ?>

			<?php if ($hotel_name == 'alphavit') { ?>
			<a href="/">
         <?php if ($lang == 'ru-RU') { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/upload/iblock/d1b/d1b1e8c3055a989a8cd04e904e27b28a.png">
		 <?php } else { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/upload/iblock/0fb/0fb5fde51cc6072c05320dedc44bb17a.png">
			<?php } ?>
			</a>
			<?php } ?>

			<?php if ($hotel_name == 'regina') { ?>
			<a href="/">
         <?php if ($lang == 'ru-RU') { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/upload/iblock/ff6/ff6f3fe7b9f38f9fc0c930d112c86705.png">
		 <?php } else { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/upload/iblock/818/8184c9a38feb7e5df52ed58e8a62d793.png">
			<?php } ?>
			</a>
			<?php } ?>

			<?php if ($hotel_name == 'eurasia') { ?>
			<a href="/">
         <?php if ($lang == 'ru-RU') { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/images/eurasia_logo.png">
		 <?php } else { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/upload/iblock/a9c/a9c35d8be7b025c5f4a37ee08129f499.png">
			<?php } ?>
			</a>
			<?php } ?>

			<?php if ($hotel_name == 'vintage') { ?>
			<a href="/">
         <?php if ($lang == 'ru-RU') { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/upload/iblock/02c/02c0934c57071889415d6beec7d230be.png" style="height:50px; margin-top:12px;">
		 <?php } else { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/upload/iblock/14f/14f7e03048bae791a319ea12a03c997f.png">
			<?php } ?>
			</a>
			<?php } ?>

			<?php if ($hotel_name == 'privilegia') { ?>
			<a href="/">
         <?php if ($lang == 'ru-RU') { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/images/logo/logo_prv_rus.png">
		 <?php } else { ?>
			<img class="mobile_logo_in_hotel" alt="" src="/images/logo/logo_prv_eng.png">
			<?php } ?>
			</a>
			<?php } ?>

      <div class="head_tel">
				<div class="head-tel_icon"></div>
<jdoc:include type="modules" name="head_contacts"/>
			</div>

      <?php if($this->countModules('lang')) : ?>
			<div class="lang">
				<div class="title">
				<?php if ($lang == 'en-GB') { ?>
				Choice of service language
				<?php } else if ($lang == 'fr-FR') { ?>
				Choisir une langue:
				<?php } else if ($lang == 'de-DE') { ?>
				Auswahl der Sprache:
				<?php } else { ?>
				Выбор языка:
				<?php } ?>
				</div>
<jdoc:include type="modules" name="lang"/>
</div>
<?php endif; ?>
		</div>

		<div class="mainmenu">
<jdoc:include type="modules" name="mainmenu"/>
		</div>
	</div>
<div class="head_shadow"></div>
	<a href="tel:88003330205"><img class="mobile_tel" src="images/icon_tel.png" alt=""></a>
	<div class="mobile_lang">
<jdoc:include type="modules" name="lang"/>
	</div>



<?php if (($parentId == '113') or ($parentId == '253') or ($parentId == '337') or ($parentId == '411')) { ?>

<div class="hotel_slides<?php if ($_SERVER[REQUEST_URI]!=="/") echo ' -inner'; ?>">
<div class="wrap">
		<script>
		$(function() {
$('.display_form_m').click(function() {
 				$('body,html').animate({
					scrollTop: 0
				}, 1);


});

$('.display_form_m').on('click', function(){
    var $that = $(this),
        nc = $that.next('.b-hotel-logo').length,
        block = nc ? $that.next('.b-hotel-logo') : $that.parent('.b-hotel-logo');
    block.slideToggle(function(){
	        $('.display_form_m',block).add(block.prev('.display_form_m'))
        .text(block.is(':visible') ? '<?php echo $hidden_form; ?>' : '<?php echo $search_room; ?>');
	if($(".b-hotel-logo").css('display') == 'block') {
		$('body').addClass('display_f');
		} else {
			$('body').removeClass('display_f');
		}
    });


});
});

			</script>

<div class="display_form_m"><?php echo $search_room; ?></div>

    <?php if ($_SERVER[REQUEST_URI]=="/") {?>
    <jdoc:include type="modules" name="order_travelline"/>
    <?php }  ?>

		<div class="hotel_menu">
			<div class="hotel_menu_inner">
    <jdoc:include type="modules" name="submenu"/>
			</div>
		</div>
</div>
<jdoc:include type="modules" name="head_hotel"/>

</div>
  <div class="shadow_border"></div>

    <?php if ($_SERVER[REQUEST_URI]!=="/") {?>
        <jdoc:include type="modules" name="order_travelline"/>
    <?php }  ?>

			<div class="hotel_menu_mobile">
 <jdoc:include type="modules" name="submenu"/>
			</div>

  <?php } ?>

<?php if (($pageClassSuffix == 'mainpage') and ($view == 'item')) { ?>
  <div class="w-slideshow main_slider">
	<div class="wrap">

		<script>
		$(function() {
$('.display_form_m').click(function() {
 				$('body,html').animate({
					scrollTop: 0
				}, 1);


});

$('.display_form_m').on('click', function(){
    var $that = $(this),
        nc = $that.next('.b-search-form').length,
        block = nc ? $that.next('.b-search-form') : $that.parent('.b-search-form');
    block.slideToggle(function(){
	        $('.display_form_m',block).add(block.prev('.display_form_m'))
        .text(block.is(':visible') ? '<?php echo $hidden_form; ?>' : '<?php echo $search_room; ?>');
	if($(".b-search-form").css('display') == 'block') {
		$('body').addClass('display_f');
		} else {
			$('body').removeClass('display_f');
		}
    });


});
});

			</script>

<div class="display_form_m"><?php echo $search_room; ?></div>
	</div>
		<jdoc:include type="modules" name="mainpage_slider"/>

	</div>

    <div class="main-form">
        <jdoc:include type="modules" name="mainpage_order"/>
    </div>

<?php if (false) {
// Отключил pkl
?>
	<div class="sp_offers"
         <?php if ($lang == 'ru-RU') { ?>
         id="offers_rus"
         <?php } ?>
         >
      <div class="wrap">
		<a href="javascript:void(0)" class="b-hotel-group-links__arrow sp_offers__arrow_left" title=""></a>
		<a href="javascript:void(0)" class="b-hotel-group-links__arrow sp_offers__arrow_right" title=""></a>
		<jdoc:include type="modules" name="mainpage_offers"/>
		</div>
	</div>
<?php } ?>

  <?php } ?>

<?php if (($pageClassSuffix == 'mainpage') and ($view == 'item')) { ?>
  <div class="content">
	<div class="wrap">
<?php } else { ?>
  <div class="content no_rel_cont">
	<div class="wrap">
<?php } ?>

<?php if (($parentId == '113') or ($parentId == '253') or ($parentId == '337') or ($parentId == '411')) { ?>
		<div class="left_cont">
		<jdoc:include type="modules" name="sidebar_hotel"/>
		<jdoc:include type="modules" name="virtual_tour"/>
		<?php if (($pageClassSuffix == 'hotel_descr') or ($pageClassSuffix == 'cafe') or ($pageClassSuffix == 'cafe hidden_l')) { } else { ?>
		<jdoc:include type="modules" name="sidebar_hotel_offer"/>
		<?php } ?>
        <jdoc:include type="modules" name="visa"/>
        <jdoc:include type="modules" name="transfer"/>
        <jdoc:include type="modules" name="sidebar_bottom"/>
		</div>
		<div class="right_cont">
			<div class="hotel_details">
            <jdoc:include type="modules" name="breadcrumbs"/>
				<jdoc:include type="component" />
		</div>
		</div>

<?php } else if (($pageClassSuffix == 'mainpage') and ($view == 'item')) { ?>
				<jdoc:include type="component" />
<?php } else { ?>
	<div class="other_page_mobile">
<div class="left_cont left_cont_other_page">

		<script>
		$(function() {
$('.display_form_m').click(function() {
 				$('body,html').animate({
					scrollTop: 0
				}, 1);


});

$('.display_form_m').on('click', function(){
    var $that = $(this),
        nc = $that.next('.b-search-form').length,
        block = nc ? $that.next('.b-search-form') : $that.parent('.b-search-form');
    block.slideToggle(function(){
	        $('.display_form_m',block).add(block.prev('.display_form_m'))
        .text(block.is(':visible') ? '<?php echo $hidden_form; ?>' : '<?php echo $search_room; ?>');
	if($(".b-search-form").css('display') == 'block') {
		$('body').addClass('display_f');
		} else {
			$('body').removeClass('display_f');
		}
    });


});
});
</script>

<div class="display_form_m"><span>Найти номера</span></div>


		<jdoc:include type="modules" name="mainpage_order"/>
                  <jdoc:include type="modules" name="transfer" style="title"/>
                  <jdoc:include type="modules" name="sidebar" style="title"/>
        <jdoc:include type="modules" name="sidebar_bottom"/>
            </div>

		<div class="right_cont">
			<?php if ($view != 'qlue404') { ?>
            <jdoc:include type="modules" name="breadcrumbs"/>
			<?php } ?>
			<?php if ($option != 'com_k2') { ?>
			<jdoc:include type="message" />
			<?php } ?>
				<jdoc:include type="component" />
        <jdoc:include type="modules" name="under_content" style="title"/>
	<?php if ($page_suf == 'contacts_page') { ?>
 <div class="b-category__item b-category_type_contacts clearfix">
        <div class="b-shadow-borders b-shadow-borders_side_left"></div>

        <div class="b-shadow-borders b-shadow-borders_side_right"></div>
		<iframe border="0" width="709" height="340" src="<?php echo JRoute::_('index.php?option=com_content&view=article&id=9&tmpl=component&pag=1'); ?>">

		</iframe>
<script type="text/javascript">
$(document).ready(function () {
        $(".map_mobile_contacts_page").colorbox({iframe: true, fastIframe: false, fixed:true,	scrolling:false, opacity:'1', scalePhotos:true,  width:'84%', height:'300px', maxWidth:'380px'});
    });
	</script>
		<a href="<?php echo JRoute::_('index.php?option=com_content&view=article&id=9&tmpl=component&pag=7'); ?>" class="map_mobile_contacts_page"><img src="images/map_mobile.jpg" alt=""></a>
</div>


	<?php } ?>
    </div>
	 </div>

<?php } ?>

</div>
</div>
	<div class="clear_sep"></div>

	<div class="hotels_slides">
		<div class="wrap">
			<a href="javascript:void(0)" class="b-hotel-group-links__arrow b-hotel-group-links__arrow_left" title=""></a>
			<a href="javascript:void(0)" class="b-hotel-group-links__arrow b-hotel-group-links__arrow_right" title=""></a>
			<div class="b-hotel-group-links__container">
     <jdoc:include type="modules" name="hotels_bottom" style="title"/>
			</div>
		</div>
	</div>


	<div class="footer_menu">
		<div class="wrap">
     <jdoc:include type="modules" name="footmenu"/>
		</div>
	</div>
	<div class="footer">
		<div class="wrap">
	     <jdoc:include type="modules" name="footer"/>
		</div>

	</div>


</div>
	 <jdoc:include type="modules" name="counters" style="title"/>

    <script type="text/javascript" src="js/readmore.js"></script>
	<script type="text/javascript">
	{
if (screen.width < 900)
		$('.page_hotel_descr').readmore({
			maxHeight: 220,
			moreLink: '<a class="readmone_mobile" href="#"><?php echo $expand; ?></a>',
			lessLink: '<a class="readmone_mobile readmone_hide" href="#"><?php echo $hidden_form; ?></a>',
			speed: 100,
			heightMargin: 16
		});
	}

	{
if (screen.width < 900)
		$('.mainpage_block1, .do_video, .posle_video').readmore({
			maxHeight: 200,
			moreLink: '<a class="readmone_mobile" href="#"><?php echo $expand; ?></a>',
			lessLink: '<a class="readmone_mobile readmone_hide" href="#"><?php echo $hidden_form; ?></a>',
			speed: 100,
			heightMargin: 16
					});
	}

	</script>



  <?php
  $page_main_search = parse_url($_SERVER['REQUEST_URI']);
  if (strpos($page_main_search['path'],"/reservation") !== false) : ?>
<style>
.lt-xbutton-main-wrapper, .lt-label.lt-label-event.lt-online, .lt-widget-wrap.lt-rating-on.lt-widget-hidden.lt-dragable.lt-internal.lt-ui-draggable, .lt-widget-wrap.lt-dragable.lt-internal.lt-ui-draggable.lt-forward {
  display: none !important;
 }
</style>
<?php endif; ?>
</body>
</html>
