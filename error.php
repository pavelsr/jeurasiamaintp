<?php
defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$this->language  = $doc->language;
$this->direction = $doc->direction;

$lang = JRequest::getVar('lang', null); 
?>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
<script src="http://hms.travelline.ru/js/tl-mobile.js" type="text/javascript" async></script>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo $this->title; ?> <?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8'); ?></title>
<meta name="viewport" content="width=1000">

<link rel="shortcut icon" href="/images/favicon.ico">
<link rel="stylesheet" type="text/css" href="/templates/main/css/template.css">
<link rel="stylesheet" type="text/css" href="/templates/main/css/style.css">
<link rel="stylesheet" href="/js/nivo/nivo-slider.css" media="screen">

    <script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/js/jquery.carouFredSel-6.2.1-packed.js" defer></script>
    <script type="text/javascript" src="/js/nivo/jquery.nivo.slider.pack.js" defer></script>
    <script type="text/javascript" src="/js/colorbox/jquery.colorbox-min.js" defer></script>
    <script type="text/javascript" src="/js/jquery-ui-1.10.4.custom.js" defer></script>
    <script type="text/javascript" src="/js/main.min.js" defer></script>

</head>
<body>

<div class="main b-page">

	<div class="head">
		<div class="wrap">
			<?php if ($lang == 'en-GB') { ?>
			<a href="/en/"><img class="logo_h" alt="" src="/images/hotel-group-logo.png"></a>
			<?php } else if ($lang == 'fr-FR') { ?>
			<a href="/fr/"><img alt="" src="/images/hotel-group-logo.png" class="logo_h"></a>
			<?php } else if ($lang == 'de-DE') { ?>
			<a href="/de/"><img alt="" src="images/hotel-group-logo.png" class="logo_h"></a>
			<?php } else { ?>
			<a href="/"><img class="logo_h" alt="" src="/images/logo.png"></a>
			<?php } ?>
			<div class="head_tel">
				<div class="head-tel_icon"></div>
 	     <?php echo $doc->getBuffer('modules', 'head_contacts', array('style' => 'none')); ?>
			</div>
			<div class="head_social">
				<div class="head_skype">
 	     <?php echo $doc->getBuffer('modules', 'skype', array('style' => 'none')); ?>
				</div>
				<div class="soc_link">
 	     <?php echo $doc->getBuffer('modules', 'social_links', array('style' => 'none')); ?>
				</div>
			</div>
			<div class="lang">
				<div class="title">
				<?php if ($lang == 'en-GB') { ?>
				Choice of service language
				<?php } else if ($lang == 'fr-FR') { ?>
				Choisir une langue:
				<?php } else if ($lang == 'de-DE') { ?>
				Auswahl der Sprache:
				<?php } else { ?>
				Выбор языка:
				<?php } ?>
				</div>
 	     <?php echo $doc->getBuffer('modules', 'lang', array('style' => 'none')); ?>
</div>
		</div>	
			
		<div class="mainmenu">
 	     <?php echo $doc->getBuffer('modules', 'mainmenu', array('style' => 'none')); ?>
		</div>
	</div>
<div class="head_shadow"></div>
	
	
  
  <div class="content">
	<div class="wrap">

					<!-- Begin Content -->
					<h1 class="page-header"><?php echo JText::_('JERROR_LAYOUT_PAGE_NOT_FOUND'); ?></h1>
					<div class="well">
						<div class="row-fluid">
							<div class="span6">
								<p><strong><?php echo JText::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST'); ?></strong></p>
								<p><?php echo JText::_('JERROR_LAYOUT_NOT_ABLE_TO_VISIT'); ?></p>
								<ul>
									<li><?php echo JText::_('JERROR_LAYOUT_AN_OUT_OF_DATE_BOOKMARK_FAVOURITE'); ?></li>
									<li><?php echo JText::_('JERROR_LAYOUT_MIS_TYPED_ADDRESS'); ?></li>
									<li><?php echo JText::_('JERROR_LAYOUT_SEARCH_ENGINE_OUT_OF_DATE_LISTING'); ?></li>
									<li><?php echo JText::_('JERROR_LAYOUT_YOU_HAVE_NO_ACCESS_TO_THIS_PAGE'); ?></li>
								</ul>
							</div>
							<div class="span6">
								<?php if (JModuleHelper::getModule('search')) : ?>
									<p><strong><?php echo JText::_('JERROR_LAYOUT_SEARCH'); ?></strong></p>
									<p><?php echo JText::_('JERROR_LAYOUT_SEARCH_PAGE'); ?></p>
									<?php echo $doc->getBuffer('module', 'search'); ?>
								<?php endif; ?>
								<p><?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?></p>
								<p><a href="<?php echo $this->baseurl; ?>/index.php" class="btn"><i class="icon-home"></i> <?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a></p>
							</div>
						</div>
						<hr />
						<p><?php echo JText::_('JERROR_LAYOUT_PLEASE_CONTACT_THE_SYSTEM_ADMINISTRATOR'); ?></p>
						<blockquote>
							<span class="label label-inverse"><?php echo $this->error->getCode(); ?></span> <?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8');?>
						</blockquote>
					</div>

</div>
</div>	
	<div class="clear_sep"></div>
	
	<div class="hotels_slides">
		<div class="wrap">
			<a href="javascript:void(0)" class="b-hotel-group-links__arrow b-hotel-group-links__arrow_left" title=""></a>
			<a href="javascript:void(0)" class="b-hotel-group-links__arrow b-hotel-group-links__arrow_right" title=""></a>
			<div class="b-hotel-group-links__container">
 	     <?php echo $doc->getBuffer('modules', 'hotels_bottom', array('style' => 'title')); ?>
			</div>
		</div>
	</div>
	
	
	<div class="footer_menu">
		<div class="wrap">
 	     <?php echo $doc->getBuffer('modules', 'footmenu', array('style' => 'none')); ?>
		</div>
	</div>
	<div class="footer">
		<div class="wrap">
		
 	     <?php echo $doc->getBuffer('modules', 'footer', array('style' => 'none')); ?>
		
		</div>
	
	</div>
	
  </div>

</div>

</body>
</html>
