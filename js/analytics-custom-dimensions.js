var hotelsDimensions = false;
(function ($) {
    // Hotels custom demensions
    hotelsDimensions = (function(){
      var data = {
        hrental_id : '',
        hrental_pagetype : '',
        hrental_totalvalue : 0,
        hrental_startdate : '',
        hrental_enddate : '',
      };
      /**
       * Инициализация
       */
      function init() {
        var jsonData = $.cookie('hotelsDimension'); 
        if (jsonData !== undefined) {
          data = $.parseJSON(jsonData);
        }
        data.hrental_pagetype = window.location.href;
        pushData();
      }
      /**
       * Сохранить данные в кукисы
       */
      function save() {
        var jsonData = JSON.stringify(data);
        $.cookie('hotelsDimension', jsonData, { expires: 3650 });
      }
      
      /**
       * Обновить переменную данных
       * @param string name
       * @param string value
       * @returns undefined
       */
      function updateValue(name,value) {
        data[name] = value;
        save();
      };
      
      /**
       * Получить данные переменной
       * @param string name
       * @returns mixed
       */
      function getValue(name) {
        return data[name];
      }
      
      /**
       * Вывести все данные 
       * @returns undefined
       */
      function showData() {
        console.log(data);
      }
      
      /**
       * Обновить идентификатор текущего отеля используя класс body текущей страницы
       * @returns Boolean
       */
      function updateHotelId() {
        var hotels = {
          dynasty : '174',
          vintage : '911',
          privilegia : '6022',
          regina : '177',
          alfavit : '1726',
          amsterdam : '176',
        }
        if ($('body').attr('class') === undefined) {
          return false;
        }
        var bodyClassList = $('body').attr('class').split(/\s+/);
        $(bodyClassList).each(function(index, className){
          if (className.indexOf('hot_') === 0) {
            var hotelName = className.replace('hot_','');
            if (hotels[hotelName] !== undefined) {
              updateValue('hrental_id',hotels[hotelName]);
            }
          }
        });
      }
      
      /**
       * Обновить переменную hrental_totalvalue
       * @param string amount
       * @returns undefined
       */
      function updateAmount(amount) {
        amount = parseInt(amount);
        if (amount > 0) {
          data.hrental_totalvalue = amount;
          save();
        }
      }
      
      /**
       * Отправить данные в таг менеджер
       * @returns undefined
       */
      function pushData() {
        $.each(data,function( index, value ) {
          if (index == 'hrental_totalvalue') {
            value = value + '.00';
          } 
          dataLayer.push({index: value});
        });
      }
      
      init();

      return {
        getValue : getValue,
        updateValue : updateValue,
        save : save,
        showData : showData,
        updateHotelId : updateHotelId,
        updateAmount : updateAmount,
      };
    }());
    
    $(document).ready(function(){
      hotelsDimensions.updateHotelId();
    });
    
})(jQuery);

